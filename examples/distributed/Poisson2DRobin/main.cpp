#include <algorithm>
#include <chrono>
#include <gmsh.h>
#include <gmshfem/DistributedField.h>
#include <gmshfem/Formulation.h>
#include <gmshfem/GmshFem.h>
#include <gmshfem/MatrixFactory.h>
#include <gmshfem/VectorFactory.h>
#include <thread>

using namespace gmshfem;
using namespace gmshfem::common;
using namespace gmshfem::problem;
using namespace gmshfem::domain;
using namespace gmshfem::field;
using namespace gmshfem::function;
using namespace gmshfem::post;
using namespace gmshfem::equation;
using namespace gmshfem::msg;


void genMesh(double L, double H, unsigned nx, unsigned ny, double lc, unsigned overlapSize)
{
  namespace geo = gmsh::model::geo;


  gmsh::model::add("genMesh");
  double dx = L / nx;
  double dy = H / ny;
  double delta = lc * overlapSize;
  if(delta > dx || delta > dy)
    throw Exception("Overlap too big!");


  // points[i][j] with i the x axis and j the y axis. Each subdomain has 4 points in a direction, but offset of 3
  // because of sharing
  std::vector< std::vector< int > > points(1 + 3 * nx, std::vector< int >(1 + 3 * ny));

  // All points
  for(unsigned i = 0; i < nx; ++i) {
    for(unsigned j = 0; j < ny; ++j) {
      double x = i * dx;
      double y = j * dy;
      points[3 * i][3 * j] = gmsh::model::geo::addPoint(x, y, 0., lc);
      points[3 * i + 1][3 * j] = gmsh::model::geo::addPoint(x + delta, y, 0., lc);
      points[3 * i + 2][3 * j] = gmsh::model::geo::addPoint(x + dx - delta, y, 0., lc);

      points[3 * i][3 * j + 1] = gmsh::model::geo::addPoint(x, y + delta, 0., lc);
      points[3 * i + 1][3 * j + 1] = gmsh::model::geo::addPoint(x + delta, y + delta, 0., lc);
      points[3 * i + 2][3 * j + 1] = gmsh::model::geo::addPoint(x + dx - delta, y + delta, 0., lc);

      points[3 * i][3 * j + 2] = gmsh::model::geo::addPoint(x, y + dy - delta, 0., lc);
      points[3 * i + 1][3 * j + 2] = gmsh::model::geo::addPoint(x + delta, y + dy - delta, 0., lc);
      points[3 * i + 2][3 * j + 2] = gmsh::model::geo::addPoint(x + dx - delta, y + dy - delta, 0., lc);

      if(j == (ny - 1)) {
        points[3 * i][3 * j + 3] = gmsh::model::geo::addPoint(x, y + dy, 0., lc);
        points[3 * i + 1][3 * j + 3] = gmsh::model::geo::addPoint(x + delta, y + dy, 0., lc);
        points[3 * i + 2][3 * j + 3] = gmsh::model::geo::addPoint(x + dx - delta, y + dy, 0., lc);
      }

      if(i == (nx - 1)) {
        points[3 * i + 3][3 * j] = gmsh::model::geo::addPoint(x + dx, y, 0., lc);
        points[3 * i + 3][3 * j + 1] = gmsh::model::geo::addPoint(x + dx, y + delta, 0., lc);
        points[3 * i + 3][3 * j + 2] = gmsh::model::geo::addPoint(x + dx, y + dy - delta, 0., lc);
        if(j == (ny - 1)) {
          points[3 * i + 3][3 * j + 3] = gmsh::model::geo::addPoint(x + dx, y + dy, 0., lc);
        }
      }
    }
  }

  // There are 24 lines on each subdomain. Some are duplicated!
  struct SubdomainLines {
    // All ids in order [bottom, right, up, left]
    // Lines are always pointing to the right or upward
    int inner[4]; // Inne square
    int outer[4]; // Interior parts (corner excluded) of outer square
    int cornerLines[16]; // 4 * cornerId + edgeId,
  };

  std::vector< std::vector< SubdomainLines > > lines(nx, std::vector< SubdomainLines >(ny));


  // All lines
  for(unsigned i = 0; i < nx; ++i) {
    for(unsigned j = 0; j < ny; ++j) {
      auto &sub = lines[i][j];
      sub.inner[0] = geo::addLine(points[3 * i + 1][3 * j + 1], points[3 * i + 2][3 * j + 1]);
      sub.inner[1] = geo::addLine(points[3 * i + 2][3 * j + 1], points[3 * i + 2][3 * j + 2]);
      sub.inner[2] = geo::addLine(points[3 * i + 1][3 * j + 2], points[3 * i + 2][3 * j + 2]);
      sub.inner[3] = geo::addLine(points[3 * i + 1][3 * j + 1], points[3 * i + 1][3 * j + 2]);

      // Always create bottom boundary, give it to previous subdomain
      sub.outer[0] = geo::addLine(points[3 * i + 1][3 * j], points[3 * i + 2][3 * j]);
      if(j > 0)
        lines[i][j - 1].outer[2] = sub.outer[0];

      // If I'm at the end, I create the last ones, otherwise I'll read them.
      if(i == (nx - 1))
        sub.outer[1] = geo::addLine(points[3 * i + 3][3 * j + 1], points[3 * i + 3][3 * j + 2]);
      if(j == (ny - 1))
        sub.outer[2] = geo::addLine(points[3 * i + 1][3 * j + 3], points[3 * i + 2][3 * j + 3]);

      sub.outer[3] = geo::addLine(points[3 * i][3 * j + 1], points[3 * i][3 * j + 2]);
      if(i > 0)
        lines[i - 1][j].outer[1] = sub.outer[3];


      // Always define the inner corners

      // Bottom left: right and up parts
      sub.cornerLines[1] = geo::addLine(points[3 * i + 1][3 * j], points[3 * i + 1][3 * j + 1]);
      sub.cornerLines[2] = geo::addLine(points[3 * i][3 * j + 1], points[3 * i + 1][3 * j + 1]);

      // Bottom right: up  and left parts
      sub.cornerLines[4 + 2] = geo::addLine(points[3 * i + 2][3 * j + 1], points[3 * i + 3][3 * j + 1]);
      sub.cornerLines[4 + 3] = geo::addLine(points[3 * i + 2][3 * j], points[3 * i + 2][3 * j + 1]);

      // top right: down  and left parts
      sub.cornerLines[8 + 0] = geo::addLine(points[3 * i + 2][3 * j + 2], points[3 * i + 3][3 * j + 2]);
      sub.cornerLines[8 + 3] = geo::addLine(points[3 * i + 2][3 * j + 2], points[3 * i + 2][3 * j + 3]);

      // top left: down and right parts
      sub.cornerLines[12 + 0] = geo::addLine(points[3 * i][3 * j + 2], points[3 * i + 1][3 * j + 2]);
      sub.cornerLines[12 + 1] = geo::addLine(points[3 * i + 1][3 * j + 2], points[3 * i + 1][3 * j + 3]);

      // Shared parts of corners
      // Left: always create, share if i > 0
      sub.cornerLines[3] = geo::addLine(points[3 * i][3 * j + 0], points[3 * i][3 * j + 1]);
      sub.cornerLines[12 + 3] = geo::addLine(points[3 * i][3 * j + 2], points[3 * i][3 * j + 3]);
      if(i > 0) {
        lines[i - 1][j].cornerLines[4 + 1] = sub.cornerLines[3];
        lines[i - 1][j].cornerLines[8 + 1] = sub.cornerLines[12 + 3];
      }

      // Bottom: always create
      sub.cornerLines[0] = geo::addLine(points[3 * i][3 * j + 0], points[3 * i + 1][3 * j + 0]);
      sub.cornerLines[4] = geo::addLine(points[3 * i + 2][3 * j + 0], points[3 * i + 3][3 * j + 0]);
      if(j > 0) {
        lines[i][j - 1].cornerLines[12 + 2] = sub.cornerLines[0];
        lines[i][j - 1].cornerLines[8 + 2] = sub.cornerLines[4];
      }

      // IF i'm the last col, add the right
      if(i == (nx - 1)) {
        sub.cornerLines[4 + 1] = geo::addLine(points[3 * i + 3][3 * j + 0], points[3 * i + 3][3 * j + 1]);
        sub.cornerLines[8 + 1] = geo::addLine(points[3 * i + 3][3 * j + 2], points[3 * i + 3][3 * j + 3]);
      }

      if(j == (ny - 1)) {
        sub.cornerLines[12 + 2] = geo::addLine(points[3 * i + 0][3 * j + 3], points[3 * i + 1][3 * j + 3]);
        sub.cornerLines[8 + 2] = geo::addLine(points[3 * i + 2][3 * j + 3], points[3 * i + 3][3 * j + 3]);
      }
    }
  }

  // Make the surfaces
  struct SubdomaineFaces {
    int inner;
    int corner[4];
    int edges[4];
  };

  std::vector< std::vector< SubdomaineFaces > > surfaces(nx, std::vector< SubdomaineFaces >(ny));

  for(unsigned i = 0; i < nx; ++i) {
    for(unsigned j = 0; j < ny; ++j) {
      auto &theseLines = lines[i][j];
      int loop = geo::addCurveLoop({theseLines.inner[0], theseLines.inner[1], -theseLines.inner[2], -theseLines.inner[3]});
      surfaces[i][j].inner = geo::addPlaneSurface({loop});

      // "Edges"
      loop = geo::addCurveLoop({theseLines.outer[0], theseLines.cornerLines[7], -theseLines.inner[0], -theseLines.cornerLines[1]});
      surfaces[i][j].edges[0] = geo::addPlaneSurface({loop});

      loop = geo::addCurveLoop({-theseLines.inner[1], theseLines.cornerLines[6], theseLines.outer[1], -theseLines.cornerLines[8]});
      surfaces[i][j].edges[1] = geo::addPlaneSurface({loop});

      loop = geo::addCurveLoop({theseLines.inner[2], theseLines.cornerLines[11], -theseLines.outer[2], -theseLines.cornerLines[13]});
      surfaces[i][j].edges[2] = geo::addPlaneSurface({loop});

      loop = geo::addCurveLoop({theseLines.inner[3], -theseLines.cornerLines[12], -theseLines.outer[3], theseLines.cornerLines[2]});
      surfaces[i][j].edges[3] = geo::addPlaneSurface({loop});

      // "Corners"
      for(int offset = 0; offset < 16; offset += 4) {
        loop = geo::addCurveLoop({theseLines.cornerLines[offset + 0], theseLines.cornerLines[offset + 1], -theseLines.cornerLines[offset + 2], -theseLines.cornerLines[offset + 3]});
        surfaces[i][j].corner[offset / 4] = geo::addPlaneSurface({loop});
      }
    }
  }


  gmsh::model::geo::synchronize();

  gmsh::model::mesh::generate();
  //gmsh::write("full_mesh.msh");


  for(unsigned i = 0; i < nx; ++i) {
    for(unsigned j = 0; j < ny; ++j) {
      //gmsh::model::removePhysicalGroups();

      std::vector< int > inner, overlap, boundary, coupling;
      // Fill interior, overlap and boundary
      inner.push_back(surfaces[i][j].inner);
      for(int k = 0; k < 4; ++k) {
        inner.push_back(surfaces[i][j].corner[k]);
        inner.push_back(surfaces[i][j].edges[k]);
      }

      if(i > 0) {
        overlap.push_back(surfaces[i - 1][j].edges[1]);
        overlap.push_back(surfaces[i - 1][j].corner[1]);
        overlap.push_back(surfaces[i - 1][j].corner[2]);

        // Left bnd
        coupling.push_back(lines[i - 1][j].inner[1]);
        coupling.push_back(lines[i - 1][j].cornerLines[4 + 3]);
        coupling.push_back(lines[i - 1][j].cornerLines[8 + 3]);

        if(j != 0)
          coupling.push_back(lines[i - 1][j - 1].cornerLines[8 + 3]);
        if(j != (ny - 1))
          coupling.push_back(lines[i - 1][j + 1].cornerLines[4 + 3]);
      }
      else {
        boundary.push_back(lines[i][j].outer[3]);
        boundary.push_back(lines[i][j].cornerLines[3]);
        boundary.push_back(lines[i][j].cornerLines[12 + 3]);

        if(j != 0)
          boundary.push_back(lines[i][j - 1].cornerLines[15]);
        if(j != (ny - 1))
          boundary.push_back(lines[i][j + 1].cornerLines[3]);
      }

      if(j > 0) {
        overlap.push_back(surfaces[i][j - 1].edges[2]);
        overlap.push_back(surfaces[i][j - 1].corner[2]);
        overlap.push_back(surfaces[i][j - 1].corner[3]);

        // Bottom bnd
        coupling.push_back(lines[i][j - 1].inner[2]);
        coupling.push_back(lines[i][j - 1].cornerLines[8 + 0]);
        coupling.push_back(lines[i][j - 1].cornerLines[12 + 0]);

        if(i != 0)
          coupling.push_back(lines[i - 1][j - 1].cornerLines[8]);
        if(i != (nx - 1))
          coupling.push_back(lines[i + 1][j - 1].cornerLines[12]);
      }
      else {
        boundary.push_back(lines[i][j].outer[0]);
        boundary.push_back(lines[i][j].cornerLines[0]);
        boundary.push_back(lines[i][j].cornerLines[4]);

        if(i != 0)
          boundary.push_back(lines[i - 1][j].cornerLines[4]);
        if(i != (nx - 1))
          boundary.push_back(lines[i + 1][j].cornerLines[0]);
      }

      if(i != (nx - 1)) {
        overlap.push_back(surfaces[i + 1][j].edges[3]);
        overlap.push_back(surfaces[i + 1][j].corner[3]);
        overlap.push_back(surfaces[i + 1][j].corner[0]);

        // Right bnd
        coupling.push_back(lines[i + 1][j].inner[3]);
        coupling.push_back(lines[i + 1][j].cornerLines[1]);
        coupling.push_back(lines[i + 1][j].cornerLines[12 + 1]);

        if(j != 0)
          coupling.push_back(lines[i + 1][j - 1].cornerLines[12 + 1]);
        if(j != (ny - 1))
          coupling.push_back(lines[i + 1][j + 1].cornerLines[1]);
      }
      else {
        boundary.push_back(lines[i][j].outer[1]);
        boundary.push_back(lines[i][j].cornerLines[4 + 1]);
        boundary.push_back(lines[i][j].cornerLines[8 + 1]);

        if(j != 0)
          boundary.push_back(lines[i][j - 1].cornerLines[8 + 1]);
        if(j != (ny - 1))
          boundary.push_back(lines[i][j + 1].cornerLines[4 + 1]);
      }

      if(j != (ny - 1)) {
        overlap.push_back(surfaces[i][j + 1].edges[0]);
        overlap.push_back(surfaces[i][j + 1].corner[1]);
        overlap.push_back(surfaces[i][j + 1].corner[0]);

        // top bnd
        coupling.push_back(lines[i][j + 1].inner[0]);
        coupling.push_back(lines[i][j + 1].cornerLines[2]);
        coupling.push_back(lines[i][j + 1].cornerLines[4 + 2]);

        if(i != 0)
          coupling.push_back(lines[i - 1][j + 1].cornerLines[4 + 2]);
        if(i != (nx - 1))
          coupling.push_back(lines[i + 1][j + 1].cornerLines[2]);
      }
      else {
        // top bnd
        boundary.push_back(lines[i][j].outer[2]);
        boundary.push_back(lines[i][j].cornerLines[8 + 2]);
        boundary.push_back(lines[i][j].cornerLines[12 + 2]);

        if(i != 0)
          boundary.push_back(lines[i - 1][j].cornerLines[10]);
        if(i != (nx - 1))
          boundary.push_back(lines[i + 1][j].cornerLines[14]);
      }

      /*
      Corner of overlaps
      */
      if(i != 0 && j != 0) {
        // SW corner
        overlap.push_back(surfaces[i - 1][j - 1].corner[2]);
      }
      if(i != 0 && j != (ny - 1)) {
        // NW corner
        overlap.push_back(surfaces[i - 1][j + 1].corner[1]);
      }
      if(i != (nx - 1) && j != 0) {
        // SE corner
        overlap.push_back(surfaces[i + 1][j - 1].corner[3]);
      }
      if(i != (nx - 1) && j != (ny - 1)) {
        // NE corner
        overlap.push_back(surfaces[i + 1][j + 1].corner[0]);
      }

      gmsh::model::geo::synchronize();

      gmsh::model::addPhysicalGroup(2, inner, 1 + 100 * (i + j * nx));
      gmsh::model::addPhysicalGroup(2, overlap, 2 + 100 * (i + j * nx));
      gmsh::model::addPhysicalGroup(1, coupling, 3 + 100 * (i + j * nx));
      gmsh::model::addPhysicalGroup(1, boundary, 4 + 100 * (i + j * nx));

      gmsh::model::geo::synchronize();

      //gmsh::write("mesh_" + std::to_string(i) + "_" + std::to_string(j) + ".msh");
    }
  }

  gmsh::write("full_mesh.msh");
}

int main(int argc, char **argv)
{
  using namespace std::chrono_literals;

  GmshFem gmshFem(argc, argv);
  unsigned int MPI_Rank = gmshFem.getMPIRank();
  unsigned MPI_Size = gmshFem.getMPISize();


  int order = 1;
  gmshFem.userDefinedParameter(order, "order");
  unsigned nx{2}, ny{1};
  gmshFem.userDefinedParameter(nx, "nx");
  gmshFem.userDefinedParameter(ny, "ny");
  if(MPI_Size != nx * ny)
    throw Exception("Must run with same number of subdomains and ranks! (Got " + std::to_string(MPI_Size) + ")");

  int overlapSize = 1;
  gmshFem.userDefinedParameter(overlapSize, "overlapSize");

  double h = 0.01;
  gmshFem.userDefinedParameter(h, "h");

  int iDom = MPI_Rank % nx;
  int jDom = MPI_Rank / nx;


  /****
     * Gmsh part
     ****/
  if(MPI_Rank == 0) {
    genMesh(1, 1, nx, ny, h, overlapSize);
  }
  gmshFem.BarrierMPI();

  //gmsh::open("mesh_" + std::to_string(iDom) + "_" + std::to_string(jDom) + ".msh");
  gmsh::open("full_mesh.msh");
  gmsh::model::mesh::createEdges();
  gmsh::model::mesh::createFaces();

  auto analytical = 1 + x< std::complex< double > >() + 2 * x< std::complex< double > >() * y< std::complex< double > >() + pow(y< std::complex< double > >(), 3);

  Formulation< std::complex< double > > formulation("formulation_" + std::to_string(MPI_Rank));
  Domain omega(2, 1 + 100 * (iDom + jDom * nx));
  Domain overlap(2, 2 + 100 * (iDom + jDom * nx));
  Domain coupling(1, 3 + 100 * (iDom + jDom * nx));
  Domain boundary(1, 4 + 100 * (iDom + jDom * nx));

  DistributedField< std::complex< double >, Form::Form0 > v("v", omega, overlap, FunctionSpaceTypeForm0::HierarchicalH1, order);

  // "Convection" coefficient
  double alpha = 1;
  gmshFem.userDefinedParameter(alpha, "alpha");

  formulation.integral(grad(dof(v)), grad(tf(v)), v.domain(), "Gauss6");
  formulation.integral(alpha * dof(v), tf(v), coupling, "Gauss6");
  formulation.integral(6. * y< std::complex< double > >(), (tf(v)), v.domain(), "Gauss6");

  v.addConstraint(boundary, analytical);

  formulation.pre();
  formulation.preDistributed();
  formulation.assemble();
  formulation.solveDistributed(system::DISTRIBUTED_SOLVE_TYPE::ORAS, false);


  auto errorLoc = real(integrate((v - analytical) * conj(v - analytical), omega, "Gauss6"));
  auto solNormLoc = real(integrate((analytical) * conj(analytical), omega, "Gauss6"));
  double l2_rel_error = gmshFem.reductionSum(errorLoc) / gmshFem.reductionSum(solNormLoc);

  if(MPI_Rank == 0)
    msg::print << "L2 relative error in the whole domain: " << l2_rel_error << msg::endl;
  save(v, omega, "v_" + std::to_string(MPI_Rank));
  save(analytical, omega, "sol_" + std::to_string(MPI_Rank));

  double tol = 1e-2;
  if(order > 1)
    tol = 1e-4;
  if(l2_rel_error > 1e-2) {
    throw common::Exception("Error above tolerance : " + std::to_string(tol));
  }

  return 0;
}
