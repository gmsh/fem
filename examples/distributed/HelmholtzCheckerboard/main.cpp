#include <PetscInterface.h>
#include <algorithm>
#include <chrono>
#include <gmsh.h>
#include <gmshfem/CSVio.h>
#include <gmshfem/DistributedField.h>
#include <gmshfem/Formulation.h>
#include <gmshfem/GmshFem.h>
#include <gmshfem/MatrixFactory.h>
#include <gmshfem/Solver.h>
#include <gmshfem/SolverMonitor.h>
#include <gmshfem/VectorFactory.h>
#include <thread>

using namespace gmshfem;
using namespace gmshfem::common;
using namespace gmshfem::problem;
using namespace gmshfem::domain;
using namespace gmshfem::field;
using namespace gmshfem::function;
using namespace gmshfem::post;
using namespace gmshfem::equation;
using namespace gmshfem::msg;


class MonitorL2Error : public gmshfem::system::AbstractMonitor
{
 private:
  Domain dom;
  DistributedField< std::complex< double >, Form::Form0 > &field;
  DistributedField< std::complex< double >, Form::Form0 > &UnknownField;
  double normSol;
  Formulation< std::complex< double > > &formulation;

 public:
  MonitorL2Error(Domain dom, DistributedField< std::complex< double >, Form::Form0 > &field, DistributedField< std::complex< double >, Form::Form0 > &UnknownField, Formulation< std::complex< double > > &form) :
    dom(dom), field(field), UnknownField(UnknownField), formulation(form)
  {
    // COMPUTE SOL NORM, REDUCE, IN CALLBACK: set values into field
    normSol = gmshfem::common::GmshFem::reductionSum(real(integrate((field)*conj(field), dom, "Gauss6")));
    if(gmshfem::common::GmshFem::getMPIRank() == 0)
      msg::info << "Initalize L2 monitor with " << normSol << msg::endl;
  }

  virtual PetscErrorCode Monitor(KSP ksp, PetscInt n, PetscReal rnorm)
  {
    auto MPI_Rank = gmshfem::common::GmshFem::getMPIRank();

    Vec x;
    VecType t;
    PetscInt size;
    KSPBuildSolution(ksp, nullptr, &x);
    VecGetType(x, &t);
    std::vector< std::complex< double > > values;

    values.resize(formulation.getNumberOfUnknownDof());
    formulation.getDistributedContext()->readScatteredData(values, x);
    formulation.setSolutionIntoFields(values);

    double normErr = gmshfem::common::GmshFem::reductionSum(real(integrate((field - UnknownField) * conj(field - UnknownField), dom, "Gauss6")));


    if(MPI_Rank == 0)
      msg::info << "Monitor called " << n << " and rnorm " << rnorm << ". Rel L² error is: " << sqrt(normErr / normSol) << msg::endl;

    return PETSC_SUCCESS;
  }
};

void genMesh(double L, double H, unsigned nx, unsigned ny, double lc, unsigned overlapSize, std::vector< std::pair< double, double > > sources)
{
  namespace geo = gmsh::model::geo;
  std::vector< int > sourcePoints, sourceDoms;
  CSVio out_source_domains("source_subdomains");


  gmsh::model::add("genMesh");
  double dx = L / nx;
  double dy = H / ny;
  double delta = lc * overlapSize;
  if(delta > dx || delta > dy)
    throw Exception("Overlap too big!");


  // points[i][j] with i the x axis and j the y axis. Each subdomain has 4 points in a direction, but offset of 3
  // because of sharing
  std::vector< std::vector< int > > points(1 + 3 * nx, std::vector< int >(1 + 3 * ny));

  // All points
  for(unsigned i = 0; i < nx; ++i) {
    for(unsigned j = 0; j < ny; ++j) {
      double x = i * dx;
      double y = j * dy;
      points[3 * i][3 * j] = gmsh::model::geo::addPoint(x, y, 0., lc);
      points[3 * i + 1][3 * j] = gmsh::model::geo::addPoint(x + delta, y, 0., lc);
      points[3 * i + 2][3 * j] = gmsh::model::geo::addPoint(x + dx - delta, y, 0., lc);

      points[3 * i][3 * j + 1] = gmsh::model::geo::addPoint(x, y + delta, 0., lc);
      points[3 * i + 1][3 * j + 1] = gmsh::model::geo::addPoint(x + delta, y + delta, 0., lc);
      points[3 * i + 2][3 * j + 1] = gmsh::model::geo::addPoint(x + dx - delta, y + delta, 0., lc);

      points[3 * i][3 * j + 2] = gmsh::model::geo::addPoint(x, y + dy - delta, 0., lc);
      points[3 * i + 1][3 * j + 2] = gmsh::model::geo::addPoint(x + delta, y + dy - delta, 0., lc);
      points[3 * i + 2][3 * j + 2] = gmsh::model::geo::addPoint(x + dx - delta, y + dy - delta, 0., lc);

      if(j == (ny - 1)) {
        points[3 * i][3 * j + 3] = gmsh::model::geo::addPoint(x, y + dy, 0., lc);
        points[3 * i + 1][3 * j + 3] = gmsh::model::geo::addPoint(x + delta, y + dy, 0., lc);
        points[3 * i + 2][3 * j + 3] = gmsh::model::geo::addPoint(x + dx - delta, y + dy, 0., lc);
      }

      if(i == (nx - 1)) {
        points[3 * i + 3][3 * j] = gmsh::model::geo::addPoint(x + dx, y, 0., lc);
        points[3 * i + 3][3 * j + 1] = gmsh::model::geo::addPoint(x + dx, y + delta, 0., lc);
        points[3 * i + 3][3 * j + 2] = gmsh::model::geo::addPoint(x + dx, y + dy - delta, 0., lc);
        if(j == (ny - 1)) {
          points[3 * i + 3][3 * j + 3] = gmsh::model::geo::addPoint(x + dx, y + dy, 0., lc);
        }
      }
    }
  }

  // There are 24 lines on each subdomain. Some are duplicated!
  struct SubdomainLines {
    // All ids in order [bottom, right, up, left]
    // Lines are always pointing to the right or upward
    int inner[4]; // Inne square
    int outer[4]; // Interior parts (corner excluded) of outer square
    int cornerLines[16]; // 4 * cornerId + edgeId,
  };

  std::vector< std::vector< SubdomainLines > > lines(nx, std::vector< SubdomainLines >(ny));


  // All lines
  for(unsigned i = 0; i < nx; ++i) {
    for(unsigned j = 0; j < ny; ++j) {
      auto &sub = lines[i][j];
      sub.inner[0] = geo::addLine(points[3 * i + 1][3 * j + 1], points[3 * i + 2][3 * j + 1]);
      sub.inner[1] = geo::addLine(points[3 * i + 2][3 * j + 1], points[3 * i + 2][3 * j + 2]);
      sub.inner[2] = geo::addLine(points[3 * i + 1][3 * j + 2], points[3 * i + 2][3 * j + 2]);
      sub.inner[3] = geo::addLine(points[3 * i + 1][3 * j + 1], points[3 * i + 1][3 * j + 2]);

      // Always create bottom boundary, give it to previous subdomain
      sub.outer[0] = geo::addLine(points[3 * i + 1][3 * j], points[3 * i + 2][3 * j]);
      if(j > 0)
        lines[i][j - 1].outer[2] = sub.outer[0];

      // If I'm at the end, I create the last ones, otherwise I'll read them.
      if(i == (nx - 1))
        sub.outer[1] = geo::addLine(points[3 * i + 3][3 * j + 1], points[3 * i + 3][3 * j + 2]);
      if(j == (ny - 1))
        sub.outer[2] = geo::addLine(points[3 * i + 1][3 * j + 3], points[3 * i + 2][3 * j + 3]);

      sub.outer[3] = geo::addLine(points[3 * i][3 * j + 1], points[3 * i][3 * j + 2]);
      if(i > 0)
        lines[i - 1][j].outer[1] = sub.outer[3];


      // Always define the inner corners

      // Bottom left: right and up parts
      sub.cornerLines[1] = geo::addLine(points[3 * i + 1][3 * j], points[3 * i + 1][3 * j + 1]);
      sub.cornerLines[2] = geo::addLine(points[3 * i][3 * j + 1], points[3 * i + 1][3 * j + 1]);

      // Bottom right: up  and left parts
      sub.cornerLines[4 + 2] = geo::addLine(points[3 * i + 2][3 * j + 1], points[3 * i + 3][3 * j + 1]);
      sub.cornerLines[4 + 3] = geo::addLine(points[3 * i + 2][3 * j], points[3 * i + 2][3 * j + 1]);

      // top right: down  and left parts
      sub.cornerLines[8 + 0] = geo::addLine(points[3 * i + 2][3 * j + 2], points[3 * i + 3][3 * j + 2]);
      sub.cornerLines[8 + 3] = geo::addLine(points[3 * i + 2][3 * j + 2], points[3 * i + 2][3 * j + 3]);

      // top left: down and right parts
      sub.cornerLines[12 + 0] = geo::addLine(points[3 * i][3 * j + 2], points[3 * i + 1][3 * j + 2]);
      sub.cornerLines[12 + 1] = geo::addLine(points[3 * i + 1][3 * j + 2], points[3 * i + 1][3 * j + 3]);

      // Shared parts of corners
      // Left: always create, share if i > 0
      sub.cornerLines[3] = geo::addLine(points[3 * i][3 * j + 0], points[3 * i][3 * j + 1]);
      sub.cornerLines[12 + 3] = geo::addLine(points[3 * i][3 * j + 2], points[3 * i][3 * j + 3]);
      if(i > 0) {
        lines[i - 1][j].cornerLines[4 + 1] = sub.cornerLines[3];
        lines[i - 1][j].cornerLines[8 + 1] = sub.cornerLines[12 + 3];
      }

      // Bottom: always create
      sub.cornerLines[0] = geo::addLine(points[3 * i][3 * j + 0], points[3 * i + 1][3 * j + 0]);
      sub.cornerLines[4] = geo::addLine(points[3 * i + 2][3 * j + 0], points[3 * i + 3][3 * j + 0]);
      if(j > 0) {
        lines[i][j - 1].cornerLines[12 + 2] = sub.cornerLines[0];
        lines[i][j - 1].cornerLines[8 + 2] = sub.cornerLines[4];
      }

      // IF i'm the last col, add the right
      if(i == (nx - 1)) {
        sub.cornerLines[4 + 1] = geo::addLine(points[3 * i + 3][3 * j + 0], points[3 * i + 3][3 * j + 1]);
        sub.cornerLines[8 + 1] = geo::addLine(points[3 * i + 3][3 * j + 2], points[3 * i + 3][3 * j + 3]);
      }

      if(j == (ny - 1)) {
        sub.cornerLines[12 + 2] = geo::addLine(points[3 * i + 0][3 * j + 3], points[3 * i + 1][3 * j + 3]);
        sub.cornerLines[8 + 2] = geo::addLine(points[3 * i + 2][3 * j + 3], points[3 * i + 3][3 * j + 3]);
      }
    }
  }

  // Make the surfaces
  struct SubdomaineFaces {
    int inner;
    int corner[4];
    int edges[4];
  };

  std::vector< std::vector< SubdomaineFaces > > surfaces(nx, std::vector< SubdomaineFaces >(ny));

  for(unsigned i = 0; i < nx; ++i) {
    for(unsigned j = 0; j < ny; ++j) {
      auto &theseLines = lines[i][j];
      int loop = geo::addCurveLoop({theseLines.inner[0], theseLines.inner[1], -theseLines.inner[2], -theseLines.inner[3]});
      surfaces[i][j].inner = geo::addPlaneSurface({loop});

      // "Edges"
      loop = geo::addCurveLoop({theseLines.outer[0], theseLines.cornerLines[7], -theseLines.inner[0], -theseLines.cornerLines[1]});
      surfaces[i][j].edges[0] = geo::addPlaneSurface({loop});

      loop = geo::addCurveLoop({-theseLines.inner[1], theseLines.cornerLines[6], theseLines.outer[1], -theseLines.cornerLines[8]});
      surfaces[i][j].edges[1] = geo::addPlaneSurface({loop});

      loop = geo::addCurveLoop({theseLines.inner[2], theseLines.cornerLines[11], -theseLines.outer[2], -theseLines.cornerLines[13]});
      surfaces[i][j].edges[2] = geo::addPlaneSurface({loop});

      loop = geo::addCurveLoop({theseLines.inner[3], -theseLines.cornerLines[12], -theseLines.outer[3], theseLines.cornerLines[2]});
      surfaces[i][j].edges[3] = geo::addPlaneSurface({loop});

      // "Corners"
      for(int offset = 0; offset < 16; offset += 4) {
        loop = geo::addCurveLoop({theseLines.cornerLines[offset + 0], theseLines.cornerLines[offset + 1], -theseLines.cornerLines[offset + 2], -theseLines.cornerLines[offset + 3]});
        surfaces[i][j].corner[offset / 4] = geo::addPlaneSurface({loop});
      }
    }
  }

  // Add source
  for(auto &pos : sources) {
    double x = pos.first;
    double y = pos.second;
    int iSrc = static_cast< int >(x / dx);
    int jSrc = static_cast< int >(y / dy);
    int srcDom = iSrc + jSrc * nx;
    sourceDoms.push_back(srcDom);
    sourcePoints.push_back(geo::addPoint(pos.first, pos.second, 0, lc));

    out_source_domains << srcDom << csv::endl;

    gmsh::model::geo::synchronize();


    double xloc = x - iSrc * dx;
    double yloc = y - jSrc * dy;

    auto point = sourcePoints.back();
    if(xloc > delta && xloc < (dx - delta)) {
      // Inner
      if(yloc > delta && yloc < (dy - delta)) {
        gmsh::model::mesh::embed(0, {point}, 2, surfaces[iSrc][jSrc].inner);
      }
      else if(yloc < delta) {
        // S edge
        gmsh::model::mesh::embed(0, {point}, 2, surfaces[iSrc][jSrc].edges[0]);
      }
      else if(yloc > (dy - delta)) {
        // N edge
        gmsh::model::mesh::embed(0, {point}, 2, surfaces[iSrc][jSrc].edges[2]);
      }
      else {
        throw common::Exception("Bad source position");
      }
    }
    else if(xloc < delta) {
      // W edge
      if(yloc > delta && yloc < (dy - delta)) {
        gmsh::model::mesh::embed(0, {point}, 2, surfaces[iSrc][jSrc].edges[3]);
      }
      else if(yloc < delta) {
        // SW corner
        gmsh::model::mesh::embed(0, {point}, 2, surfaces[iSrc][jSrc].corner[0]);
      }
      else if(yloc > (dy - delta)) {
        // NW corner
        gmsh::model::mesh::embed(0, {point}, 2, surfaces[iSrc][jSrc].corner[3]);
      }
      else {
        throw common::Exception("Bad source position");
      }
    }
    else if(xloc > (dx - delta)) {
      // E edge
      if(yloc > delta && yloc < (dy - delta)) {
        gmsh::model::mesh::embed(0, {point}, 2, surfaces[iSrc][jSrc].edges[1]);
      }
      else if(yloc < delta) {
        // SE corner
        gmsh::model::mesh::embed(0, {point}, 2, surfaces[iSrc][jSrc].corner[1]);
      }
      else if(yloc > (dy - delta)) {
        // NE corner
        gmsh::model::mesh::embed(0, {point}, 2, surfaces[iSrc][jSrc].corner[2]);
      }
      else {
        throw common::Exception("Bad source position");
      }
    }
  }

  //int src = geo::addPoint(xsrc, ysrc, 0, lc);
  //int iSrc = nx / 2;
  //int jSrc = ny / 2;

  //gmsh::model::geo::synchronize();
  //gmsh::model::mesh::embed(0, {src}, 2, surfaces[0][0].inner);

  gmsh::model::mesh::generate();
  //gmsh::write("full_mesh.msh");


  for(unsigned i = 0; i < nx; ++i) {
    for(unsigned j = 0; j < ny; ++j) {
      //gmsh::model::removePhysicalGroups();

      std::vector< int > inner, overlap, boundary;
      // Fill interior, overlap and boundary
      inner.push_back(surfaces[i][j].inner);
      for(int k = 0; k < 4; ++k) {
        inner.push_back(surfaces[i][j].corner[k]);
        inner.push_back(surfaces[i][j].edges[k]);
      }

      if(i > 0) {
        overlap.push_back(surfaces[i - 1][j].edges[1]);
        overlap.push_back(surfaces[i - 1][j].corner[1]);
        overlap.push_back(surfaces[i - 1][j].corner[2]);

        // Left bnd
        boundary.push_back(lines[i - 1][j].inner[1]);
        boundary.push_back(lines[i - 1][j].cornerLines[4 + 3]);
        boundary.push_back(lines[i - 1][j].cornerLines[8 + 3]);
      }
      else {
        boundary.push_back(lines[i][j].outer[3]);
        boundary.push_back(lines[i][j].cornerLines[3]);
        boundary.push_back(lines[i][j].cornerLines[12 + 3]);
      }

      if(j > 0) {
        overlap.push_back(surfaces[i][j - 1].edges[2]);
        overlap.push_back(surfaces[i][j - 1].corner[2]);
        overlap.push_back(surfaces[i][j - 1].corner[3]);

        // Bottom bnd
        boundary.push_back(lines[i][j - 1].inner[2]);
        boundary.push_back(lines[i][j - 1].cornerLines[8 + 0]);
        boundary.push_back(lines[i][j - 1].cornerLines[12 + 0]);
      }
      else {
        boundary.push_back(lines[i][j].outer[0]);
        boundary.push_back(lines[i][j].cornerLines[0]);
        boundary.push_back(lines[i][j].cornerLines[4]);
      }

      if(i != (nx - 1)) {
        overlap.push_back(surfaces[i + 1][j].edges[3]);
        overlap.push_back(surfaces[i + 1][j].corner[3]);
        overlap.push_back(surfaces[i + 1][j].corner[0]);

        // Right bnd
        boundary.push_back(lines[i + 1][j].inner[3]);
        boundary.push_back(lines[i + 1][j].cornerLines[4 + 1]);
        boundary.push_back(lines[i + 1][j].cornerLines[8 + 1]);
      }
      else {
        boundary.push_back(lines[i][j].outer[1]);
        boundary.push_back(lines[i][j].cornerLines[4 + 1]);
        boundary.push_back(lines[i][j].cornerLines[8 + 1]);
      }

      if(j != (ny - 1)) {
        overlap.push_back(surfaces[i][j + 1].edges[0]);
        overlap.push_back(surfaces[i][j + 1].corner[1]);
        overlap.push_back(surfaces[i][j + 1].corner[0]);

        // top bnd
        boundary.push_back(lines[i][j + 1].inner[0]);
        boundary.push_back(lines[i][j + 1].cornerLines[2]);
        boundary.push_back(lines[i][j + 1].cornerLines[4 + 2]);
      }
      else {
        // top bnd
        boundary.push_back(lines[i][j].outer[2]);
        boundary.push_back(lines[i][j].cornerLines[8 + 2]);
        boundary.push_back(lines[i][j].cornerLines[12 + 2]);
      }
      gmsh::model::geo::synchronize();

      /*
      Corner of overlaps
      */
      if(i != 0 && j != 0) {
        // SW corner
        overlap.push_back(surfaces[i - 1][j - 1].corner[2]);
      }
      if(i != 0 && j != (ny - 1)) {
        // NW corner
        overlap.push_back(surfaces[i - 1][j + 1].corner[1]);
      }
      if(i != (nx - 1) && j != 0) {
        // SE corner
        overlap.push_back(surfaces[i + 1][j - 1].corner[3]);
      }
      if(i != (nx - 1) && j != (ny - 1)) {
        // NE corner
        overlap.push_back(surfaces[i + 1][j + 1].corner[0]);
      }

      gmsh::model::addPhysicalGroup(2, inner, 1 + 100 * (i + j * nx));
      gmsh::model::addPhysicalGroup(2, overlap, 2 + 100 * (i + j * nx));
      gmsh::model::addPhysicalGroup(1, boundary, 3 + 100 * (i + j * nx));

      /*gmsh::model::addPhysicalGroup(2, {3}, 2, "overlap");
      gmsh::model::addPhysicalGroup(1, {l110}, 1, "gammaLat");
      gmsh::model::addPhysicalGroup(1, {l89, l910}, 2, "gammaTop");
      gmsh::model::addPhysicalGroup(1, {l12, l23}, 3, "gammaBottom");
      gmsh::model::addPhysicalGroup(0, {srcLoc}, 1, "src");*/
      for(int srcIdx = 0; srcIdx < sourceDoms.size(); ++srcIdx) {
        if(i + nx * j == sourceDoms[srcIdx])
          gmsh::model::addPhysicalGroup(0, {sourcePoints[srcIdx]}, srcIdx + 1);
      }

      gmsh::model::geo::synchronize();

      //gmsh::write("mesh_" + std::to_string(i) + "_" + std::to_string(j) + ".msh");
    }
  }

  gmsh::write("full_mesh.msh");
}
/*
void solveFullProblem(unsigned nCells, unsigned order, double k)
{
  gmsh::open("full_mesh.msh");
  Domain omega("omega");
  Domain gamma("gamma");
  Domain src("src");

  Formulation< std::complex< double > > formulation("formulation_full");
  Field< std::complex< double >, Form::Form0 > v("v", omega, FunctionSpaceTypeForm0::HierarchicalH1, order);

  formulation.integral(grad(dof(v)), grad(tf(v)), v.domain(), "Gauss6");
  formulation.integral(-k * k * dof(v), tf(v), v.domain(), "Gauss6");
  formulation.integral(std::complex< double >(0, 1) * k * dof(v), tf(v), gamma, "Gauss6");
  formulation.integral(-1, tf(v), src, "Gauss6");

  formulation.pre();
  formulation.assemble();
  formulation.solve(false);
  save(v, omega, "v_full");
}*/

int main(int argc, char **argv)
{
  using namespace std::chrono_literals;

  GmshFem gmshFem(argc, argv);
  unsigned int MPI_Rank = gmshFem.getMPIRank();
  unsigned MPI_Size = gmshFem.getMPISize();

  int numSolsToSave = 1;
  gmshFem.userDefinedParameter(numSolsToSave, "numSolsToSave");

  int order = 1;
  gmshFem.userDefinedParameter(order, "order");
  unsigned nx{2}, ny{1};

  //if(MPI_Size != 2)
  //  throw Exception("Must run with 2 ranks! (Got " + std::to_string(MPI_Size) + ")");
  gmshFem.userDefinedParameter(nx, "nx");
  gmshFem.userDefinedParameter(ny, "ny");
  int overlapSize = 1;
  gmshFem.userDefinedParameter(overlapSize, "overlapSize");

  double wavelength = 0.05;
  gmshFem.userDefinedParameter(wavelength, "wavelength");

  double k = 2 * 3.1415 / wavelength;

  int iDom = MPI_Rank % nx;
  int jDom = MPI_Rank / nx;


  /****
     * Gmsh part
     ****/
  std::vector<double> x_src = {0.17, 0.22, 0.27, 0.32, 0.37, 0.42, 0.47, 0.52, 0.57, 0.62, 0.67, 0.72, 0.77, 0.82, 0.87};
  std::vector< std::pair< double, double > > sources;
  for (double x: x_src)
    {
      sources.push_back({x, 0.8});
      sources.push_back({x, 0.7});
      sources.push_back({x, 0.1});
    }

  if(MPI_Rank == 0) {
    genMesh(1, 1, nx, ny, wavelength / 10, overlapSize, sources);
  }
  gmshFem.BarrierMPI();

  std::vector< int > srcDomains, sourcesOwned;
  CSVio out_source_domains("source_subdomains", ';', OpeningMode::Reading);
  int tmp;
  while((out_source_domains >> tmp).isEOF() == false) {
    srcDomains.push_back(tmp);
    if(tmp == MPI_Rank)
      sourcesOwned.push_back(srcDomains.size() - 1);
    if(MPI_Rank == 0)
      msg::debug << "Source " << srcDomains.size() - 1 << " goes in " << tmp << msg::endl;
  }



  //gmsh::open("mesh_" + std::to_string(iDom) + "_" + std::to_string(jDom) + ".msh");
  gmsh::open("full_mesh.msh");
  gmsh::model::mesh::createEdges();
  gmsh::model::mesh::createFaces();


  Formulation< std::complex< double > > formulation("formulation_" + std::to_string(MPI_Rank));
  Domain omega(2, 1 + 100 * (iDom + jDom * nx));
  Domain overlap(2, 2 + 100 * (iDom + jDom * nx));
  Domain boundary(1, 3 + 100 * (iDom + jDom * nx));

  DistributedField< std::complex< double >, Form::Form0 > v("v", omega, overlap, FunctionSpaceTypeForm0::HierarchicalH1, order);

  formulation.integral(grad(dof(v)), grad(tf(v)), v.domain(), "Gauss6");
  std::complex< double > k2Complex(k * k, -k * k * 0.0);
  std::complex< double > im(0., 1.0);
  formulation.integral(-k2Complex * dof(v), tf(v), v.domain(), "Gauss6");
  formulation.integral(im * k * dof(v), tf(v), boundary, "Gauss6");
  for(int srcIdx = 0; srcIdx < srcDomains.size(); ++srcIdx) {
    formulation.setCurrentRHS(srcIdx);

    if(std::find(sourcesOwned.begin(), sourcesOwned.end(), srcIdx) != sourcesOwned.end()) {
      Domain thisSrc(0, srcIdx + 1);
      formulation.integral(-1, tf(v), thisSrc, "Gauss6");
    }
  }

  formulation.pre();
  formulation.preDistributed();
  formulation.assemble();
  formulation.solveAllDistributed(system::DISTRIBUTED_SOLVE_TYPE::ORAS, false);

  for (unsigned kSol = 0; kSol < formulation.numRHS(); ++kSol) {
    formulation.loadSolution(kSol);
    if (kSol < numSolsToSave)
      save(v, omega, "v_rhs_" + std::to_string(kSol) + "_rank_" + std::to_string(MPI_Rank));
  }

  // Error analysis on first RHS
  formulation.loadSolution(0);
  decltype(v) v_backup = v;
  std::unique_ptr< system::AbstractMonitor > monitor = std::make_unique< MonitorL2Error >(omega, v_backup, v, formulation);
  formulation.setMonitor(std::move(monitor));
  formulation.solveDistributed(system::DISTRIBUTED_SOLVE_TYPE::ORAS, true);

  
  save(v, omega, "v_" + std::to_string(MPI_Rank));
  save(v - v_backup, omega, "delta_" + std::to_string(MPI_Rank));
  auto vnorm = real(integrate((v)*conj(v), omega, "Gauss6"));
  auto error1 = real(integrate((v - v_backup) * conj(v - v_backup), omega, "Gauss6"));

  msg::print << "L2 absolute error in domain " << MPI_Rank << " : " << error1 << " and sol has norm " << vnorm << " so rel error is " << error1 / vnorm << msg::endl;
  double v_tot = gmshFem.reductionSum(vnorm);
  double err_tot = gmshFem.reductionSum(error1);
  if(MPI_Rank == 0)
    msg::print << "L2 absolute error in domain (total)): " << err_tot << " and sol has norm " << v_tot << " so rel error is " << err_tot / v_tot << msg::endl;


  //save(v - solution, omega, "err_" + std::to_string(MPI_Rank));
  /*
    if(error1 > 1e-2) {
    throw common::Exception("Error above tolerance : " + std::to_string(error1));
  }*/

  return 0;
}
