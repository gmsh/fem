#include <algorithm>
#include <gmsh.h>
#include <gmshfem/Formulation.h>
#include <gmshfem/GmshFem.h>
#include <gmshfem/DistributedField.h>
#include <gmshfem/MatrixFactory.h>
#include <gmshfem/VectorFactory.h>
#include <chrono>
#include <thread>
#include <gmshfem/CSVio.h>
using namespace gmshfem;
using namespace gmshfem::common;
using namespace gmshfem::problem;
using namespace gmshfem::domain;
using namespace gmshfem::field;
using namespace gmshfem::function;
using namespace gmshfem::post;
using namespace gmshfem::equation;
using namespace gmshfem::msg;

void genMesh()
{
    gmsh::model::add("genMesh");

    namespace geo = gmsh::model::geo;

    std::vector<int> points, hlines, vlines, surfaces;
    std::vector<double> xs{0, 0.4, 0.5, 0.6, 1.0};
    std::vector<double> ys = xs;
    for (double y : ys)
    {
        for (double x : xs)
            points.push_back(geo::addPoint(x, y, 0));
    }

    for (unsigned j = 0; j < 5; ++j)
    {
        for (unsigned i = 0; i < 4; ++i)
        {
            hlines.push_back(geo::addLine(points[j * 5 + i], points[j * 5 + i + 1]));
                gmsh::model::geo::mesh::setTransfiniteCurve(hlines.back(), 2);

        }
    }

    for (unsigned j = 0; j < 4; ++j)
    {
        for (unsigned i = 0; i < 5; ++i)
        {
            vlines.push_back(geo::addLine(points[j * 5 + i], points[(j+1) * 5 + i]));
                gmsh::model::geo::mesh::setTransfiniteCurve(vlines.back(), 2);

        }
    }


    for (unsigned j = 0; j < 4; ++j)
    {
        for (unsigned i = 0; i < 4; ++i)
        {
            int curve = geo::addCurveLoop({hlines[4 * j + i], vlines[5 * j + i + 1], -hlines[4 * (j + 1) + i], -vlines[5 * j + i]});
            surfaces.push_back(geo::addPlaneSurface({curve}));
            geo::mesh::setTransfiniteSurface(surfaces.back());
            //geo::mesh::setRecombine(2, surfaces.back());
        }
    }


    gmsh::model::geo::synchronize();

    bool makeFourDomains = true;
    using gmsh::model::addPhysicalGroup;
    if (makeFourDomains) {
        // Each subdomain has an "omega" (1), an "overlap" (2), a part of the physical boundary (3) (overlap inc.), and a coupling boundary (4)
        // ID: above + 100*domain index
        for (int i = 0; i < 2; ++i) {
            for (int j = 0; j < 2; ++j) {
                int domIdx = 2*i+j;
                int offset = 8*i + 2*j;
                addPhysicalGroup(2, {surfaces[offset+ 0], surfaces[offset+1], surfaces[offset+4], surfaces[offset+4+1]}, 1 + 100*domIdx);
            }
        }

        // Define overlaps
        addPhysicalGroup(2, {surfaces[2], surfaces[6], surfaces[10], surfaces[9], surfaces[8]}, 2 + 100 * 0);
        addPhysicalGroup(2, {surfaces[1], surfaces[5], surfaces[9], surfaces[10], surfaces[11]}, 2 + 100 * 1);
        addPhysicalGroup(2, {surfaces[4], surfaces[5], surfaces[6], surfaces[10], surfaces[14]}, 2 + 100 * 2);
        addPhysicalGroup(2, {surfaces[5], surfaces[6], surfaces[7], surfaces[9], surfaces[13]}, 2 + 100 * 3);

        // Define physical boundaries (with overlap)
        addPhysicalGroup(1, {hlines[0], hlines[1], hlines[2], vlines[0], vlines[5],  vlines[10]}, 3 + 100 * 0);
        addPhysicalGroup(1, {hlines[1], hlines[2], hlines[3], vlines[4], vlines[9],  vlines[14]}, 3 + 100 * 1);
        addPhysicalGroup(1, {hlines[16], hlines[17], hlines[18], vlines[5], vlines[10],  vlines[15]}, 3 + 100 * 2);
        addPhysicalGroup(1, {hlines[17], hlines[18], hlines[19], vlines[9], vlines[14],  vlines[19]}, 3 + 100 * 3);

        // Define coupling boundaries
        addPhysicalGroup(1, {hlines[12], hlines[13], hlines[14], vlines[3], vlines[8],  vlines[13]}, 4 + 100 * 0);
        addPhysicalGroup(1, {hlines[13], hlines[14], hlines[15], vlines[1], vlines[6],  vlines[11]}, 4 + 100 * 1);
        addPhysicalGroup(1, {hlines[4], hlines[5], hlines[6], vlines[8], vlines[13],  vlines[18]}, 4 + 100 * 2);
        addPhysicalGroup(1, {hlines[5], hlines[6], hlines[7], vlines[6], vlines[11],  vlines[16]}, 4 + 100 * 3);
    }

    //gmsh::model::addPhysicalGroup(1, hlines);
    //gmsh::model::addPhysicalGroup(2, surfaces);
    gmsh::model::mesh::generate();

    gmsh::write("mesh.msh");
    gmsh::model::remove();
}

int main(int argc, char **argv)
{
    using namespace std::chrono_literals;

    GmshFem gmshFem(argc, argv);
    unsigned int MPI_Rank = gmshFem.getMPIRank();
    unsigned MPI_Size = gmshFem.getMPISize();
    if (MPI_Size != 4)
        throw Exception("Must run with 4 ranks! (Got " + std::to_string(MPI_Size) + ")");

    /****
     * Gmsh part
     ****/
    if (MPI_Rank == 0)
        genMesh();
    gmshFem.BarrierMPI();

    // x + 2y + 1, harmonic fct
    auto analytical = real(x<std::complex<double>>() + 2 * y<std::complex<double>>() + 1);
    gmsh::open("mesh.msh");
    gmsh::model::mesh::createEdges();
    gmsh::model::mesh::createFaces();

    Formulation<std::complex<double>> formulation("formulation_" + std::to_string(MPI_Rank));
    //*
    int shift = MPI_Rank * 100;
    Domain omega(2, 1 + shift);
    Domain overlap(2, 2 + shift);
    Domain boundary(1, 3 + shift);
    Domain coupling(1, 4 + shift);

    DistributedField<std::complex<double>, Form::Form0> v("v", omega, overlap, FunctionSpaceTypeForm0::HierarchicalH1, 1);

    formulation.integral(grad(dof(v)), grad(tf(v)), v.domain(), "Gauss6");
    formulation.integral(dof(v), tf(v), coupling, "Gauss6");


    v.addConstraint(boundary, analytical);

    formulation.pre();
    formulation.preDistributed();
    formulation.assemble();
    formulation.solveDistributed(system::DISTRIBUTED_SOLVE_TYPE::ORAS, false);

    auto error1 = real(integrate(pow(real(v - analytical), 2), omega, "Gauss6")) / real(integrate(pow(real(analytical), 2), omega, "Gauss6"));
    msg::print << "L2 absolute error in domain " << MPI_Rank << " : " << error1 << msg::endl;
    save(v, omega, "v_" + std::to_string(MPI_Rank));
    save(analytical, omega, "sol_" + std::to_string(MPI_Rank));

    if (error1 > 1e-4)
    {
        throw common::Exception("Error above tolerance : " + std::to_string(error1));
    }

    return 0;
}
