#include <algorithm>
#include <gmsh.h>
#include <gmshfem/Formulation.h>
#include <gmshfem/GmshFem.h>
#include <gmshfem/DistributedField.h>
#include <gmshfem/MatrixFactory.h>
#include <gmshfem/VectorFactory.h>
#include <chrono>
#include <thread>

using namespace gmshfem;
using namespace gmshfem::common;
using namespace gmshfem::problem;
using namespace gmshfem::domain;
using namespace gmshfem::field;
using namespace gmshfem::function;
using namespace gmshfem::post;
using namespace gmshfem::equation;
using namespace gmshfem::msg;

void genMesh(double lc, unsigned nDom, unsigned nOverlap)
{
    namespace geo = gmsh::model::geo;

    gmsh::model::add("genMesh");
    gmsh::option::setNumber("General.ExpertMode", 1);
    double L = 2;
    double domSize = L / nDom;
    double delta = nOverlap * lc;
    if (delta > domSize)
    {
        throw common::Exception("Subdomains too small!");
    }

    std::vector<int> points;
    points.reserve(nDom * 3 + 1);
    points.push_back(geo::addPoint(0, 0, 0, lc));
    for (unsigned iDom = 0; iDom < nDom; ++iDom)
    {
        double x = iDom * domSize;
        points.push_back(geo::addPoint(x + delta, 0, 0, lc));
        points.push_back(geo::addPoint(x + domSize - delta, 0, 0, lc));
        points.push_back(geo::addPoint(x + domSize, 0, 0, lc));
    }

    std::vector<int> lines;
    lines.reserve(3 * nDom);
    for (unsigned iDom = 0; iDom < nDom; ++iDom)
    {
        lines.push_back(geo::addLine(points[iDom * 3], points[iDom * 3 + 1]));
        lines.push_back(geo::addLine(points[iDom * 3 + 1], points[iDom * 3 + 2]));
        lines.push_back(geo::addLine(points[iDom * 3 + 2], points[iDom * 3 + 3]));
    }

    gmsh::model::geo::synchronize();
    gmsh::model::mesh::generate();

    for (unsigned iDom = 0; iDom < nDom; ++iDom)
    {
        gmsh::model::addPhysicalGroup(1, {lines[iDom*3], lines[iDom*3+1], lines[iDom*3+2]}, 100 * iDom + 1, "omega");
        std::vector<int> overlap;
        if (iDom > 0)
            overlap.push_back(lines[3*iDom-1]);
        if (iDom != (nDom - 1))
            overlap.push_back(lines[3*iDom+3]);
        gmsh::model::addPhysicalGroup(1, {overlap}, 100 * iDom + 2, "overlap");
        if (iDom == 0)
            gmsh::model::addPhysicalGroup(0, {points[0]}, iDom * 100 + 3, "boundary");
        if (iDom == (nDom - 1))
            gmsh::model::addPhysicalGroup(0, {points.back()}, iDom * 100 + 4, "boundary");
        gmsh::model::geo::synchronize();
    }

    // Everyone needs the full mesh (so far)
    gmsh::model::addPhysicalGroup(1, {lines}, 1234, "omega_all");
    gmsh::write("mesh_full.msh");

    gmsh::model::remove();
}

int main(int argc, char **argv)
{
    using namespace std::chrono_literals;

    GmshFem gmshFem(argc, argv);
    unsigned int MPI_Rank = gmshFem.getMPIRank();
    unsigned MPI_Size = gmshFem.getMPISize();
    //if (MPI_Size != 2)
    //    throw Exception("Must run with 2 ranks! (Got " + std::to_string(MPI_Size) + ")");

    int order = 1;
    gmshFem.userDefinedParameter(order, "order");

    double lc = 2.0 / MPI_Size / 3; // 3 elements per domain
    gmshFem.userDefinedParameter(lc, "lc");

    /****
     * Gmsh part
     ****/
    if (MPI_Rank == 0)
        genMesh(lc, MPI_Size, 1);

    std::this_thread::sleep_for(100ms);
    bool volumeSrc = false;
    gmshFem.userDefinedParameter(volumeSrc, "volumeSrc");

    // -X²/2 + X has laplacian -1, is 0 on 0 and 2 on 2
    // function::ScalarFunction<std::complex<double>> analytical = -x<std::complex<double>>()*x<std::complex<double>>()/2 + 2*x<std::complex<double>>();
    auto analytical = real(-x<std::complex<double>>() * x<std::complex<double>>() / 2 + 2 * x<std::complex<double>>());
    if (!volumeSrc)
    {
        analytical = 1 * x<std::complex<double>>();
    }
    gmsh::open("mesh_full.msh");
    gmsh::model::mesh::createEdges();

    Formulation<std::complex<double>> formulation("formulation_" + std::to_string(MPI_Rank));
    Domain omega(1, MPI_Rank * 100 + 1);
    Domain overlap(1, MPI_Rank * 100 + 2);
    Domain boundary;

    DistributedField<std::complex<double>, Form::Form0> v("v", omega, overlap, FunctionSpaceTypeForm0::HierarchicalH1, order);
    if (MPI_Rank == 0 || MPI_Rank == (MPI_Size - 1))
    {
        int localIdx = MPI_Rank == 0 ? 3 : 4;
        boundary = Domain(0, MPI_Rank * 100 + localIdx);
        v.addConstraint(boundary, x<std::complex<double>>());
    }

    formulation.integral(grad(dof(v)), grad(tf(v)), v.domain(), "Gauss6");
    if (volumeSrc)
        formulation.integral(-1, tf(v), v.domain(), "Gauss6");

    formulation.pre();
    formulation.preDistributed();
    formulation.assemble();
    gmshFem.BarrierMPI();

    formulation.solveDistributed(system::DISTRIBUTED_SOLVE_TYPE::DIRECT, false);
    save(v, omega, "v_" + std::to_string(MPI_Rank));

    auto error1 = real(integrate(pow(real(v - analytical), 2), omega, "Gauss6")) / real(integrate(pow(real(analytical), 2), omega, "Gauss6"));
    msg::print << "L2 relative error in domain " << MPI_Rank << " : " << error1 << msg::endl;
    save(analytical, omega, "v_truth_" + std::to_string(MPI_Rank));
    save(analytical - v, omega, "v_err_" + std::to_string(MPI_Rank));

    if (abs(error1) > 1e-6)
    {
        throw common::Exception("Error above tolerance. Log10 is : " + std::to_string(log10(error1)));
    }

    return 0;
}
