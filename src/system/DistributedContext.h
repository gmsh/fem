// GmshFEM - Copyright (C) 2024, B. Martin, C. Geuzaine, Université de Liège
//
// See the LICENSE.txt file for license information. Please report all
// issues on https://gitlab.onelab.info/gmsh/fem/issues

#ifndef H_GMSHFEM_DISTRIBUTEDCONTEXT
#define H_GMSHFEM_DISTRIBUTEDCONTEXT

#include "Memory.h"
#include "OmpInterface.h"
#include "Vector.h"
#include "gmshfemDefines.h"

#include <string>
#include <vector>
#include <memory>

typedef struct _p_Vec *Vec;
typedef struct _p_IS *IS;
typedef int PetscErrorCode;

namespace gmshfem::system
{

template <class T_Scalar>
class DistributedContextImpl;

template <class T_Scalar>

class DistributedContext {
    using Indices = std::vector<unsigned long long>;

    private:

     std::unique_ptr< DistributedContextImpl<T_Scalar> > _impl;
     Indices _localToGlobal;
     Indices _localIDofOwned;
     Indices _localIDofNonOwned;

     #ifdef HAVE_PETSC
     mutable IS _subdomainsIS = nullptr;
    #endif

    public:
    const Indices& localToGlobal() const;
    const Indices& localIDofOwned() const;
    const Indices& localIDofNonOwned() const;
    void readScatteredData(std::vector<T_Scalar>& values, Vec sol) const;
    #ifdef HAVE_PETSC
    PetscErrorCode getSubdomainsIS(IS* is) const;
    #endif
    DistributedContext(Indices&& localToGlobal, Indices&& owned, Indices&& nonOwned);
    ~DistributedContext();


};

};


#endif