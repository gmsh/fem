// GmshFEM - Copyright (C) 2023, B. Martin, C. Geuzaine, Université de Liège
//
// See the LICENSE.txt file for license information. Please report all
// issues on https://gitlab.onelab.info/gmsh/fem/issues

#ifndef H_GMSHFEM_SOLVER_MONITOR
#define H_GMSHFEM_SOLVER_MONITOR

typedef struct _p_KSP *KSP;

#ifdef HAVE_PETSC
#include "petscksp.h"
#endif

namespace gmshfem::system
{
  struct AbstractMonitor {
#ifdef HAVE_PETSC
    virtual PetscErrorCode Monitor(KSP ksp, PetscInt n, PetscReal rnorm) = 0;
#endif
    virtual ~AbstractMonitor() = default;
  };

} // namespace gmshfem::system

#endif