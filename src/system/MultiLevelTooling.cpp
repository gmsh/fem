// GmshFEM - Copyright (C) 2024, B. Martin, C. Geuzaine, Université de Liège
//
// See the LICENSE.txt file for license information. Please report all
// issues on https://gitlab.onelab.info/gmsh/fem/issues

#include "MultiLevelTooling.h"

#include "instantiate.h"

#ifdef HAVE_PETSC
#include "petsc.h"
#endif

namespace gmshfem::system
{
  template< class T_Scalar >
  RestrictionLowOrder< T_Scalar >::RestrictionLowOrder(problem::Formulation< T_Scalar > &formulation)
  {
    #ifdef HAVE_PETSC
    // Find all the coarse dofs that are owned.
    std::vector< dofs::UnknownDof * > ownedCoarseDofs;
    for(auto &[tag, fieldPtr] : formulation._unknownFields) {
      auto coarseSet = fieldPtr->getUnknownDofsLowOrder(1);
      auto owned = fieldPtr->getAllOwnedDofs();
      auto nonowned = fieldPtr->getNonOwnedDofs();
      for(dofs::UnknownDof *dof : coarseSet) {
        if(nonowned.find(dof) == nonowned.end()) {
          ownedCoarseDofs.push_back(dof);
        }
      }
    }
    gmshfem::msg::print << "Owned coarse dofs: " << ownedCoarseDofs.size() << gmshfem::msg::endl;
    // Create ai, aj, values
    std::vector< PetscInt > ai(ownedCoarseDofs.size() + 1, 0); // One element per row
    std::vector< PetscInt > aj(ownedCoarseDofs.size());
    std::vector< PetscScalar > values(ownedCoarseDofs.size(), 1.0); // Boolean matrix
    for(unsigned long long i = 0; i < ownedCoarseDofs.size(); ++i) {
      ai[i + 1] = ai[i] + 1;
      aj[i] = ownedCoarseDofs[i]->numGlobalDof() - 1;
    }
    auto nCols = 1;

    // Create the restriction matrix
    MatCreateMPIAIJWithArrays(PETSC_COMM_WORLD, ownedCoarseDofs.size(), formulation.getDistributedContext()->localToGlobal().size(), PETSC_DETERMINE, PETSC_DETERMINE, ai.data(), aj.data(), values.data(), &_globalRestriction);
    #endif
  }

  INSTANTIATE_CLASS(RestrictionLowOrder, 4, TEMPLATE_ARGS(std::complex< double >, std::complex< float >, double, float))

} // namespace gmshfem::system