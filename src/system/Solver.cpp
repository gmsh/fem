// GmshFEM - Copyright (C) 2019-2022, A. Royer, E. Béchet, C. Geuzaine, Université de Liège
//
// See the LICENSE.txt file for license information. Please report all
// issues on https://gitlab.onelab.info/gmsh/fem/issues

#include "Solver.h"

#include "Exception.h"
#include "MatrixFactory.h"
#include "Message.h"
#include "OmpInterface.h"
#include "PetscInterface.h"
#include "VectorFactory.h"
#include "gmshfemDefines.h"
#include "instantiate.h"
#include "Formulation.h"

#include <GmshFem.h>
#include <complex>

#if(PETSC_VERSION_MAJOR <= 3) && (PETSC_VERSION_MINOR < 9)
#define PCFactorSetMatSolverType PCFactorSetMatSolverPackage
#endif

namespace gmshfem::system
{

  // Code to use ASM preconditioner
  template< class T_Scalar >
  struct ModifySubmatrixContext {
    const std::vector< unsigned long long > *localIdOfNonOwned;
    const std::vector< unsigned long long > *localIdOfOwned;
    const std::vector< unsigned long long > *localToGlobalMap;
    MatrixFactory< T_Scalar > *A;
  };

  #ifdef HAVE_PETSC
  template< class T_Scalar >
  static PetscErrorCode modifySubMatrix(PC pc, PetscInt nsub, const IS row[], const IS col[], Mat submat[], void *ctxPtr)
  {
    ModifySubmatrixContext< T_Scalar > *ctx = static_cast< ModifySubmatrixContext< T_Scalar > * >(ctxPtr);
    if(ctx == nullptr)
      return PETSC_ERR_ARG_WRONG;

    PetscInt m, n;
    MatGetSize(submat[0], &m, &n);
    MatType type;
    MatGetType(submat[0], &type);

    PetscInt nRows;
    ISGetSize(row[0], &nRows);

    //ISRestoreIndices(row[0], &rowsIdx);


    ctx->A->modifyPetscORAS(submat[0], *ctx->localIdOfOwned, *ctx->localIdOfNonOwned, *ctx->localToGlobalMap, row[0], col[0]);

    return PetscErrorCode(0);
  };
  #endif // HAVE_PETSC

#ifdef HAVE_PETSC

  template< class T_Scalar >
  void Solver< T_Scalar >::_loadPetscSolution(Vec x, std::vector< T_Scalar > &values)
  {
    PetscInt size;
    VecGetLocalSize(x, &size);
    values.resize((isComplexWithRealPetsc() ? size / 2 : size));

    PetscScalar *array;
    VecGetArray(x, &array);

    PetscInterface< T_Scalar, PetscScalar > interface;
    interface(values, array, size);
    VecRestoreArray(x, &array);
  }

  template< class T_Scalar >
  void Solver< T_Scalar >::_loadPetscSolutionDistributed(Vec x, std::vector< T_Scalar > &values, const DistributedContext< T_Scalar > &distributedContext)
  {
    PetscInt size;
    VecGetLocalSize(x, &size);
    #warning "Handling of real Petsc on complex problems?"
    values.resize(size + distributedContext.localIDofNonOwned().size());

    distributedContext.readScatteredData(values, x);
  }

  template< class T_Scalar >
  void Solver< T_Scalar >::_loadPetscSolutionAll(Mat x, std::vector< std::vector< T_Scalar > > &values)
  {
    PetscInt m, n;
    MatGetSize(x, &m, &n);
    if (n != _b.size()) {
      throw common::Exception("Number of RHS (" + std::to_string(_b.size()) + ") does not match the number of columns in the solution matrix (" + std::to_string(n) + ")");
    }
    if (values.size() < n) {
      throw common::Exception("Not enough space allocated for the solution");
    }

    PetscScalar *array;
    MatDenseGetArray(x, &array);
    PetscInterface< T_Scalar, PetscScalar > interface;

    for(unsigned iRHS = 0; iRHS < _b.size(); ++iRHS) {
      // Get the pointer to the beginning of the iRHS-th column.
      PetscScalar *col_start = array + iRHS * m;
      values[iRHS].resize(m);
      interface(values[iRHS], col_start, m);
    }

    MatDenseRestoreArray(x, &array);
  }

  template< class T_Scalar >
  void Solver< T_Scalar >::_loadPetscSolutionAllDistributed(Mat x, std::vector< std::vector< T_Scalar > > &values, const DistributedContext< T_Scalar > &distributedContext)
  {
    #warning "Handle FEM complex + real PETSc ?"

    PetscInt m, n;
    MatGetSize(x, &m, &n);
    if(n != _b.size()) {
      throw common::Exception("Number of RHS (" + std::to_string(_b.size()) + ") does not match the number of columns in the solution matrix (" + std::to_string(n) + ")");
    }
    if(values.size() < n) {
      throw common::Exception("Not enough space allocated for the solution");
    }

    for(unsigned c = 0; c < _b.size(); ++c) {
      values[c].resize(distributedContext.localToGlobal().size());
      Vec col;
      MatDenseGetColumnVecRead(x, c, &col);
      distributedContext.readScatteredData(values[c], col);
      MatDenseRestoreColumnVecRead(x, c, &col);
    }
  }

  template< class T_Scalar >
  int Solver< T_Scalar >::_ensureCorrectOperator(Mat *A, Mat *B, bool reusePrec)
  {
    PetscFunctionBeginUser;
    
    if(!reusePrec) {
      Mat mat = _A->getPetsc();
      KSPSetOperators(_ksp, mat, mat);
      MatDestroy(&mat); // Reference counting, so we delete so that only the KSP has a reference
    }

    Mat Aptr, Bptr;
    KSPGetOperators(_ksp, &Aptr, &Bptr);
    *A = Aptr;
    *B = Bptr;

    PetscFunctionReturn(0);
  }

  template< class T_Scalar >
  int Solver< T_Scalar >::_ensureCorrectOperatorDistributed(Mat *A, Mat *B, bool reusePrec, const DistributedContext<T_Scalar>& distributedContext)
  {
    PetscFunctionBeginUser;

    if(!reusePrec) {
      Mat mat = _A->getPetscDistributed(distributedContext);
      KSPSetOperators(_kspDistributed, mat, mat);
      MatDestroy(&mat); // Reference counting, so we delete so that only the KSP has a reference
    }
    Mat Aptr, Bptr;
    KSPGetOperators(_ksp, &Aptr, &Bptr);
    *A = Aptr;
    *B = Bptr;

    PetscFunctionReturn(0);
  }
  #endif // HAVE_PETSC

  template< class T_Scalar >
  const char *Solver< T_Scalar >::defaultSparseSolver()
  {
#if defined(PETSC_HAVE_MUMPS)
    return "mumps";
#elif defined(PETSC_HAVE_MKL_PARDISO)
return "mkl_pardiso";
#elif defined(PETSC_HAVE_UMFPACK) || defined(PETSC_HAVE_SUITESPARSE)
    return "umfpack";
#else
    return "petsc";
#endif
  }


  template< class T_Scalar >
  void Solver< T_Scalar >::setMonitor(std::unique_ptr< system::AbstractMonitor > &&ptr)
  {
    _customMonitor = std::move(ptr);
  }


  template< class T_Scalar >
  void Solver< T_Scalar >::enforceRtol(std::optional< double > rtol)
  {
    _rtol = rtol;
  }

  template< class T_Scalar >
  Solver< T_Scalar >::Solver(MatrixFactory< T_Scalar > *const A, std::vector< VectorFactory< T_Scalar > > &b) :
    _A(A), _b(b)
  {
#ifdef HAVE_PETSC
    KSPCreate(PETSC_COMM_SELF, &_ksp);
    KSPAppendOptionsPrefix(_ksp, "fem_");

#endif // HAVE_PETSC
  }

  template< class T_Scalar >
  void Solver< T_Scalar >::initDistributedKSP()
  {
#ifdef HAVE_PETSC
#ifdef HAVE_MPI
    if(_kspDistributedInitialized) {
      KSPDestroy(&_kspDistributed);
    }
    KSPCreate(PETSC_COMM_WORLD, &_kspDistributed);
    _kspDistributedInitialized = true;
    KSPAppendOptionsPrefix(_kspDistributed, "fem_");
#endif
#endif // HAVE_PETSC
  }

  template< class T_Scalar >
  Solver< T_Scalar >::~Solver()
  {
    _A = nullptr;
#ifdef HAVE_PETSC
    KSPDestroy(&_ksp);
#ifdef HAVE_MPI
    if(_kspDistributedInitialized)
      KSPDestroy(&_kspDistributed);
#endif
#endif // HAVE_PETSC
  gmshfem::msg::debug << "Solver destroyed" << gmshfem::msg::endl;
  }

  template< class T_Scalar >
  static bool _isCholesky(const bool isSymetric, const bool isHermitian)
  {
    return (isSymetric && scalar::IsReal< T_Scalar >::value) || isHermitian;
  }

  template< class T_Scalar >
  common::Memory Solver< T_Scalar >::getEstimatedFactorizationMemoryUsage(bool isCholesky) const
  {
#if defined(HAVE_PETSC) && defined(PETSC_HAVE_MUMPS)
    Mat mat = _A->getPetsc();
    Mat matFac;
    MatGetFactor(mat, MATSOLVERMUMPS, (isCholesky ? MAT_FACTOR_CHOLESKY : MAT_FACTOR_LU), &matFac);
    if(isCholesky) {
      MatCholeskyFactorSymbolic(matFac, mat, NULL, NULL);
    }
    else {
      MatLUFactorSymbolic(matFac, mat, NULL, NULL, NULL);
    }
    PetscInt ival = 0;
    MatMumpsGetInfog(matFac, 16, &ival);
    return common::Memory(ival * 1e6);
#else
#warning "Estimation of factorization memory usage not available without PETSc and MUMPS"
    msg::error << "Unable to get estimated factorization memory usage without PETSc and MUMPS" << msg::endl;
    return common::Memory(0);
#endif
  }

  template< class T_Scalar >
  common::Memory Solver< T_Scalar >::getEstimatedFactorizationDistributedMemoryUsage(const DistributedContext<T_Scalar>& distributedContext, bool isCholesky, bool sum) const
  {
#if defined(HAVE_PETSC) && defined(PETSC_HAVE_MUMPS)
    Mat mat = _A->getPetscDistributed(distributedContext);
    Mat matFac;
    MatGetFactor(mat, MATSOLVERMUMPS, (isCholesky ? MAT_FACTOR_CHOLESKY : MAT_FACTOR_LU), &matFac);
    if(isCholesky) {
      MatCholeskyFactorSymbolic(matFac, mat, NULL, NULL);
    }
    else {
      MatLUFactorSymbolic(matFac, mat, NULL, NULL, NULL);
    }
    PetscInt ival = 0;
    MatMumpsGetInfog(matFac, sum ? 17 : 16, &ival); // See MUMPS manual for INFOG(16) and INFOG(17)
    return common::Memory(ival * 1e6);
#else
#warning "Estimation of factorization memory usage not available without PETSc and MUMPS"
    msg::error << "Unable to get estimated factorization memory usage without PETSc and MUMPS" << msg::endl;
    return common::Memory(0);
#endif
  }

  template< class T_Scalar >
  void Solver< T_Scalar >::solve(std::vector< T_Scalar > &values, const bool reusePreconditioner, unsigned rhsIdx)
  {
    //*******************
    // Solve :
    //    A x = b for rhs index 0
    //*******************
    try {
#ifdef HAVE_PETSC
      Vec b = _b.at(rhsIdx).getPetsc();
      Vec x;
      VecDuplicate(b, &x);
      VecScale(b, -1.0);
      solveForPETSc(b, x, reusePreconditioner);
      _loadPetscSolution(x, values);
      VecDestroy(&x);
      VecDestroy(&b);

#else
      msg::error << "Unable to solve the linear system without PETSc" << msg::endl;
#endif
    }
    catch(const std::exception &exc) {
      values.clear();
      throw;
    }
  }
  // Only implemented those if HAVE_PETSC is defined
#ifdef HAVE_PETSC
  template< class T_Scalar >
  void Solver< T_Scalar >::solveForPETSc(Vec b, Vec x, const bool reusePreconditioner)
  {
    PetscFunctionBeginUser;
    //*******************
    // Solve :
    //    A x = b for given and already allocated PETSc vectors
    //*******************
    try {

      if(_A->getModule()->name() != "A" && _A->getModule()->name() != "AFrequency") {
        throw common::Exception("A separately assembled system can not be solved");
      }

      // Check vec size
      {
        PetscInt xsize, ysize;
        PetscCallVoid(VecGetLocalSize(x, &xsize));
        PetscCallVoid(VecGetLocalSize(b, &ysize));
        if(xsize != (PetscInt)_A->size() || ysize != (PetscInt)_A->size()) {
          throw common::Exception("Input vectors x and b must have the same size as the right-hand side. Expected is" +
                                  std::to_string(_b.at(0).size()) + " but got " + std::to_string(xsize) + " and " + std::to_string(ysize));
        }
      }

      if (operatorMustBeUpdated()) {
        Mat mat = _A->getPetsc();
        PetscCallVoid(KSPSetOperators(_ksp, mat, mat));
        PetscCallVoid(MatDestroy(&mat)); // Give Ownership to the KSP
        _mustUpdateOperator = false;
      }

      PetscCallVoid(KSPSetReusePreconditioner(_ksp, reusePreconditioner ? PETSC_TRUE : PETSC_FALSE));

      PC pc;
      PetscCallVoid(KSPGetPC(_ksp, &pc));

      // use direct sparse solver by default
      PetscCallVoid(KSPSetType(_ksp, "preonly"));
      PetscCallVoid(PCSetType(pc, _forceCholesky ? "cholesky" : "lu"));
      PetscCallVoid(PCFactorSetMatSolverType(pc, defaultSparseSolver()));
      PetscCallVoid(KSPAppendOptionsPrefix(_ksp, "fem_"));
      PetscCallVoid(KSPSetFromOptions(_ksp));

      if (_rtol) {
        PetscCallVoid(KSPSetTolerances(_ksp, _rtol.value(), PETSC_DEFAULT, PETSC_DEFAULT, PETSC_DEFAULT));
      }


      PetscInt size;
      PetscCallVoid(VecGetLocalSize(b, &size));
      KSPType ksptype;
      PetscCallVoid(KSPGetType(_ksp, &ksptype));
      PCType pctype;
      PetscCallVoid(PCGetType(pc, &pctype));
      MatSolverType stype;
      PetscCallVoid(PCFactorGetMatSolverType(pc, &stype));
      msg::info << "System size: " << size << " - "
                << (ksptype ? ksptype : "none") << " "
                << (pctype ? pctype : "none") << " "
                << (stype ? stype : "none") << msg::endl;

      PetscCallVoid(KSPSolve(_ksp, b, x));

    PetscFunctionReturnVoid();
    }
    catch(const std::exception &exc) {
      msg::error << "Error in solveForPETSc: " << exc.what() << msg::endl;
      throw;
    }
  }
  #endif


  template< class T_Scalar >
  void Solver< T_Scalar >::solveAll(std::vector< std::vector< T_Scalar > > &values, const bool reusePreconditioner)
  {
//*******************
// Solve :
//    A X = B for a thin rectangular matrix B (multi RHS)
//*******************
#if !defined(HAVE_PETSC)
    throw gmshfem::common::Exception("Solving requires PETSc.");
#else
PetscFunctionBeginUser;
    try {
      // Define block matrices
      unsigned nRHS = _b.size();
      PetscInt m = _A->size();
      Mat BlockRHS, BlockY;

      PetscCallVoid(MatCreateSeqDense(MPI_COMM_SELF, m, nRHS, NULL, &BlockRHS));
      PetscCallVoid(MatDuplicate(BlockRHS, MAT_DO_NOT_COPY_VALUES, &BlockY));
      // For each RHS, copy the vector into the column of the block
      for(PetscInt j = 0; j < static_cast< PetscInt >(nRHS); j++) {
        Vec bj = _b.at(j).getPetsc();
        Vec col;
        PetscCallVoid(MatDenseGetColumnVec(BlockRHS, j, &col));
        PetscCallVoid(VecCopy(bj, col));
        PetscCallVoid(MatDenseRestoreColumnVec(BlockRHS, j, &col));
        PetscCallVoid(VecDestroy(&bj));
      }
      PetscCallVoid(MatScale(BlockRHS, -1));

      solveForPETSc(BlockRHS, BlockY, reusePreconditioner);
      // Don't set fields to the solved system; loadSolution() should be used.
      _loadPetscSolutionAll(BlockY, values);

      PetscCallVoid(MatDestroy(&BlockRHS));
      PetscCallVoid(MatDestroy(&BlockY));
    }

    catch(const std::exception &exc) {
      values.clear();
      throw;
    }
    PetscFunctionReturnVoid();
#endif
  }

#ifdef HAVE_PETSC
template< class T_Scalar >
  void Solver< T_Scalar >::solveForPETSc(Mat B, Mat X, const bool reusePreconditioner)
  {
    PetscFunctionBeginUser;
    //*******************
    // Solve :
    //    A x = b for given and already allocated PETSc vectors
    //*******************
    try {
      if(_A->getModule()->name() != "A" && _A->getModule()->name() != "AFrequency") {
        throw common::Exception("A separately assembled system can not be solved");
      }

      // Check vec size
      {
        PetscInt mx, nx, my, ny;
        PetscCallVoid(MatGetLocalSize(X, &mx, &nx));
        PetscCallVoid(MatGetLocalSize(B, &my, &ny));
        if(mx != (PetscInt)_A->size() || mx != (PetscInt)_A->size()) {
          throw common::Exception("solveForPETSc: one of the matrices has the wrong number of rows. Expected is" +
                                  std::to_string(_A->size()) + " but got " + std::to_string(mx) + " and " + std::to_string(my));
        }
        if (nx != ny) {
          throw common::Exception("solveForPETSc: the matrices have different number of columns. Expected is" +
                                  std::to_string(nx) + " but got " + std::to_string(ny));
        }
      }

      if (operatorMustBeUpdated()) {
        Mat mat = _A->getPetsc();
        PetscCallVoid(KSPSetOperators(_ksp, mat, mat));
        PetscCallVoid(MatDestroy(&mat)); // Give Ownership to the KSP
        _mustUpdateOperator = false;
      }

      PetscCallVoid(KSPSetReusePreconditioner(_ksp, reusePreconditioner ? PETSC_TRUE : PETSC_FALSE));

      PC pc;
      PetscCallVoid(KSPGetPC(_ksp, &pc));

      // use direct sparse solver by default
      PetscCallVoid(KSPSetType(_ksp, "preonly"));
      PetscCallVoid(PCSetType(pc, _forceCholesky ? "cholesky" : "lu"));
      PetscCallVoid(PCFactorSetMatSolverType(pc, defaultSparseSolver()));
      PetscCallVoid(KSPAppendOptionsPrefix(_ksp, "fem_"));
      PetscCallVoid(KSPSetFromOptions(_ksp));
      if(_rtol) {
        PetscCallVoid(KSPSetTolerances(_ksp, _rtol.value(), PETSC_DEFAULT, PETSC_DEFAULT, PETSC_DEFAULT));
      }

      PetscInt size, nrhs;
      PetscCallVoid(MatGetLocalSize(B, &size, &nrhs));
      KSPType ksptype;
      PetscCallVoid(KSPGetType(_ksp, &ksptype));
      PCType pctype;
      PetscCallVoid(PCGetType(pc, &pctype));
      MatSolverType stype;
      PetscCallVoid(PCFactorGetMatSolverType(pc, &stype));
      msg::info << "System size: " << size << " with nrhs: " << nrhs << " - "
                << (ksptype ? ksptype : "none") << " "
                << (pctype ? pctype : "none") << " "
                << (stype ? stype : "none") << msg::endl;

      PetscCallVoid(KSPMatSolve(_ksp, B, X));

    
    }
    catch(const std::exception &exc) {
      msg::error << "Error in solveForPETSc: " << exc.what() << msg::endl;
      throw;
    }
    PetscFunctionReturnVoid();
  }
#endif

  template< class T_Scalar >
  void Solver< T_Scalar >::solveDistributed(std::vector< T_Scalar > &values, system::DISTRIBUTED_SOLVE_TYPE solveType, const DistributedContext<T_Scalar>& distributedContext, const bool reusePreconditioner)
  {
    // Unpack distributed Context
    const std::vector< unsigned long long > &localIdOfOwned = distributedContext.localIDofOwned();
    const std::vector< unsigned long long > &localIdOfNonOwned = distributedContext.localIDofNonOwned();
    const std::vector< unsigned long long > &localToGlobalMap = distributedContext.localToGlobal();
    
    //*******************
    // Solve :
    //    A x = b for rhs index 0
    //*******************
    try {
#ifdef HAVE_PETSC
      if(_A->getModule()->name() != "A" && _A->getModule()->name() != "AFrequency") {
        throw common::Exception("A separately assembled system can not be solved");
      }

      PC pc;
      KSPSetReusePreconditioner(_kspDistributed, reusePreconditioner ? PETSC_TRUE : PETSC_FALSE);
      if (!reusePreconditioner) {
        PC newpc;
        PCCreate(PETSC_COMM_WORLD, &newpc); // Cleanup, to avoid, e.g., a previous ASM preconditioner
        KSPSetPC(_kspDistributed, newpc);
        PCDestroy(&newpc); // Give ownership to KSP
        pc = newpc;
      }
      Mat A, B;
      this->_ensureCorrectOperatorDistributed(&A, &B, reusePreconditioner, distributedContext);
      PetscInt m, n;
      MatGetSize(A, &m, &n);
      
      KSPGetPC(_kspDistributed, &pc);



      bool useDDM = false;
        switch(solveType)
      {
      case DISTRIBUTED_SOLVE_TYPE::DIRECT:
        KSPSetType(_kspDistributed, "preonly");
        PCSetType(pc, _forceCholesky ? "cholesky" : "lu");
        PCFactorSetMatSolverType(pc, defaultSparseSolver());
        break;
        case DISTRIBUTED_SOLVE_TYPE::RAS:
        KSPSetType(_kspDistributed, "gmres");
        PCSetType(pc, "asm");
        useDDM = true;
        break;
        case DISTRIBUTED_SOLVE_TYPE::ORAS:
        KSPSetType(_kspDistributed, "gmres");
        PCSetType(pc, "asm");
        useDDM = true;
        break;
        
      }
      PetscViewerAndFormat *vf;
      PetscViewerAndFormatCreate(PETSC_VIEWER_STDOUT_(PETSC_COMM_WORLD), PETSC_VIEWER_DEFAULT, &vf);
      // Montior res
      KSPMonitorSet(_kspDistributed, (PetscErrorCode(*)(KSP, PetscInt, PetscReal, void *))KSPMonitorResidual, vf, (PetscErrorCode(*)(void **))PetscViewerAndFormatDestroy);
      if (_customMonitor) {
        KSPMonitorSet(_kspDistributed, [](KSP ksp, PetscInt it, PetscReal rn, void * ctx)
        {system::AbstractMonitor* monitor= static_cast<system::AbstractMonitor*> (ctx); return monitor->Monitor(ksp, it, rn);},  _customMonitor.get(), nullptr);
      }

      KSPSetFromOptions(_kspDistributed);
      if (_rtol.has_value()) {
        PetscCallVoid(KSPSetTolerances(_kspDistributed, _rtol.value(), 1e-50, PETSC_DEFAULT, PETSC_DEFAULT));
      }

      Vec sol;
      Vec b = _b.at(0).getPetscDistributed(distributedContext);
      VecScale(b, -1.0);

      // Create ghosted vector
      std::vector< PetscInt > ghostsIndices;
      for(auto sharedLocalIdx : localIdOfNonOwned) {
        ghostsIndices.push_back(localToGlobalMap[sharedLocalIdx - 1] - 1);
      }
      VecCreateGhost(PETSC_COMM_WORLD, localIdOfOwned.size(), PETSC_DETERMINE, ghostsIndices.size(), ghostsIndices.data(), &sol);
      msg::debug << "Local vector with ghosts : " << localIdOfOwned.size() << " owned DOFs and " << ghostsIndices.size() << " ghosts." << msg::endl;
      PetscInt low, high;
      VecGetOwnershipRange(sol, &low, &high);
      PetscInt size;
      VecGetSize(sol, &size);
      std::vector<PetscInt> isdata(localIdOfOwned.size() + ghostsIndices.size());
      for (unsigned k = 0; k < localIdOfOwned.size(); ++k){
        isdata[k] = low + k;
      }
      for (unsigned k = 0; k < ghostsIndices.size(); ++k){
        isdata[k + localIdOfOwned.size()] = ghostsIndices[k];
      }


      if(useDDM && !reusePreconditioner) {
        IS mySubdomain;
        distributedContext.getSubdomainsIS(&mySubdomain);
        PCASMSetLocalSubdomains(pc, 1, &mySubdomain, nullptr);
      }


      KSPType ksptype;
      KSPGetType(_kspDistributed, &ksptype);
      PCType pctype;
      PCGetType(pc, &pctype);
      MatSolverType stype;
      PCFactorGetMatSolverType(pc, &stype);
      if(common::GmshFem::getMPIRank() == 0) {
        msg::info << "System size: " << size << " - "
                  << (ksptype ? ksptype : "none") << " "
                  << (pctype ? pctype : "none") << " "
                  << (stype ? stype : "none") << msg::endl;
      }


      ModifySubmatrixContext<T_Scalar> ctx;
      ctx.localIdOfNonOwned = &localIdOfNonOwned;
      ctx.localIdOfOwned = &localIdOfOwned;
      ctx.localToGlobalMap = &localToGlobalMap;
      ctx.A = _A;

  

      if (useDDM && solveType == DISTRIBUTED_SOLVE_TYPE::ORAS)
        PCSetModifySubMatrices(pc, modifySubMatrix<T_Scalar>, &ctx);

      
      KSPSetUp(_kspDistributed);
      // SUB KSP
      PetscInt nloc, firstloc;
      KSP *subksp;
      if(useDDM) {
        PCASMGetSubKSP(pc, &nloc, &firstloc, &subksp);
        msg::debug << "nloc: " << nloc << ", firstloc: " << firstloc << msg::endl;
        PC subpc;
        //subksp[0];
        KSPGetType(subksp[0], &ksptype);
        KSPGetPC(subksp[0], &subpc);
        PCSetType(subpc, _forceCholesky ? PCCHOLESKY: PCLU);
        PCGetType(subpc, &pctype);
        PCFactorSetMatSolverType(subpc, defaultSparseSolver());
        PCFactorGetMatSolverType(subpc, &stype);
        if (gmshfem::common::GmshFem::getMPIRank() == 0)
          msg::info << "Solving subdomains using : " << ksptype << ", " << (pctype ? pctype : "none") << ", " << (stype ? stype : "none") << msg::endl;
      }


      KSPSolve(_kspDistributed, b, sol);
      _loadPetscSolutionDistributed(sol, values, distributedContext);  

      VecDestroy(&sol);
      VecDestroy(&b);
#else
      msg::error << "Unable to solve the linear system without PETSc" << msg::endl;
#endif
    }
    catch(const std::exception &exc) {
      values.clear();
      throw;
    }
  }


  template< class T_Scalar >
  void Solver< T_Scalar >::solveAllDistributed(std::vector< std::vector< T_Scalar > > &values, DISTRIBUTED_SOLVE_TYPE solveType, const DistributedContext<T_Scalar>& distributedContext, const bool reusePreconditioner)
  {
    //*******************
// Solve with MPI:
//    A X = B for a thin rectangular matrix B (multi RHS)
//*******************

    // Unpack distributed Context
    const std::vector< unsigned long long > &localIdOfOwned = distributedContext.localIDofOwned();
    const std::vector< unsigned long long > &localIdOfNonOwned = distributedContext.localIDofNonOwned();
    const std::vector< unsigned long long > &localToGlobalMap = distributedContext.localToGlobal();

    auto rank = gmshfem::common::GmshFem::getMPIRank();

#if !defined(PETSC_HAVE_HPDDM) || !defined(HAVE_PETSC)
    throw gmshfem::common::Exception("Solving of multiple RHS requires PETSc with HPDDM");
#else

    try {
      if(_A->getModule()->name() != "A" && _A->getModule()->name() != "AFrequency") {
        throw common::Exception("A separately assembled system can not be solved");
      }

      PC pc;
      KSPSetReusePreconditioner(_kspDistributed, reusePreconditioner ? PETSC_TRUE : PETSC_FALSE);
      if (!reusePreconditioner) {
        PC newpc;
        PCCreate(PETSC_COMM_WORLD, &newpc); // Cleanup, to avoid, e.g., a previous ASM preconditioner
        KSPSetPC(_kspDistributed, newpc);
        PCDestroy(&newpc); // Give ownership to KSP
        pc = newpc;
      }
      Mat A, B;
      this->_ensureCorrectOperatorDistributed(&A, &B, reusePreconditioner, distributedContext);
      PetscInt m, n;
      KSPGetOperators(_kspDistributed, &A, &B);
      MatGetSize(A, &m, &n);
      
      KSPGetPC(_kspDistributed, &pc);



      KSPSetType(_kspDistributed, KSPHPDDM);


      bool useDDM = false;
        switch(solveType)
      {
      case DISTRIBUTED_SOLVE_TYPE::DIRECT:
        KSPHPDDMSetType(_kspDistributed, KSP_HPDDM_TYPE_PREONLY);
        PCSetType(pc, _forceCholesky ? "cholesky" : "lu");
        PCFactorSetMatSolverType(pc, defaultSparseSolver());
        break;
        case DISTRIBUTED_SOLVE_TYPE::RAS:
        KSPHPDDMSetType(_kspDistributed, KSP_HPDDM_TYPE_BGMRES);
        PCSetType(pc, "asm");
        useDDM = true;
        break;
        case DISTRIBUTED_SOLVE_TYPE::ORAS:
        KSPHPDDMSetType(_kspDistributed, KSP_HPDDM_TYPE_BGMRES);
        PCSetType(pc, "asm");
        useDDM = true;
        break;
        
      }


      KSPSetFromOptions(_kspDistributed);
      if (_rtol.has_value()) {
        PetscCallVoid(KSPSetTolerances(_kspDistributed, _rtol.value(), PETSC_DEFAULT, PETSC_DEFAULT, PETSC_DEFAULT));
      }
      PetscViewerAndFormat *vf;
      PetscViewerAndFormatCreate(PETSC_VIEWER_STDOUT_(PETSC_COMM_WORLD), PETSC_VIEWER_DEFAULT, &vf);
      KSPMonitorSet(_kspDistributed, (PetscErrorCode(*)(KSP, PetscInt, PetscReal, void *))KSPMonitorResidual, vf, (PetscErrorCode(*)(void **))PetscViewerAndFormatDestroy);


      // Define block matrices
      unsigned nRHS = _b.size();
      Mat BlockRHS, BlockY;
      BlockRHS = VectorFactory< T_Scalar >::generateDistributedBlockRHS(_b, distributedContext);
      MatDuplicate(BlockRHS, MAT_DO_NOT_COPY_VALUES, &BlockY);
      MatScale(BlockRHS, -1);
      std::vector<PetscReal> norms(nRHS);
      MatGetColumnNorms(BlockRHS, NORM_1, norms.data());
      if (rank == 0) {
       for (int o = 0; o < nRHS; ++o)
               PetscPrintf(PETSC_COMM_WORLD, "for rhs %d, norm is %lf.\n", o, norms.at(o));
      }


      IS mySubdomain = nullptr;
      // Setup DDM and subsolvers
      ModifySubmatrixContext<T_Scalar> ctx;
      ctx.localIdOfNonOwned = &localIdOfNonOwned;
      ctx.localIdOfOwned = &localIdOfOwned;
      ctx.localToGlobalMap = &localToGlobalMap;
      ctx.A = _A;
      if(useDDM && !reusePreconditioner) {
        distributedContext.getSubdomainsIS(&mySubdomain);
        PCASMSetLocalSubdomains(pc, 1, &mySubdomain, nullptr);
        if(solveType == DISTRIBUTED_SOLVE_TYPE::ORAS) {
          PetscCallVoid(PCSetModifySubMatrices(pc, modifySubMatrix< T_Scalar >, &ctx));
          PetscPrintf(PETSC_COMM_WORLD, "Setting modify submatrices\n");
        }
      }

      // Setup
      {
        gmshfem::common::Timer timer;
        if(rank == 0) {
          gmshfem::msg::info << "Setting up distributed solver and preconditioner..." << gmshfem::msg::endl;
        }
        timer.tick();
        PetscCallVoid(PCSetUp(pc));
        PetscCallVoid(KSPSetUp(_kspDistributed));
        timer.tock();
        if(rank == 0) {
          gmshfem::msg::info << "Setting up distributed solver and preconditioner done in " << timer << " s" << gmshfem::msg::endl;
        }
      }
      // SUB KSP
      PetscInt nLocalSubdomains, firstloc;
      KSP *subksp;
      if(useDDM) {
        PCASMGetSubKSP(pc, &nLocalSubdomains, &firstloc, &subksp);
        msg::debug << "nloc: " << nLocalSubdomains << ", firstloc: " << firstloc << msg::endl;
        PC subpc;
        KSPType ksptype;
        PCType pctype;
        MatSolverType stype;
        //subksp[0];
        KSPGetType(subksp[0], &ksptype);
        KSPGetPC(subksp[0], &subpc);
        PCSetType(subpc, _forceCholesky ? PCCHOLESKY : PCLU);
        PCFactorSetMatSolverType(subpc, "mumps");
        KSPSetFromOptions(subksp[0]);
        PCGetType(subpc, &pctype);

        PCFactorGetMatSolverType(subpc, &stype);

        if(gmshfem::common::GmshFem::getMPIRank() == 0)
          msg::info << "Solving subdomains using : " << ksptype << ", " << (pctype ? pctype : "none") << ", " << (stype ? stype : "none") << msg::endl;
      }


      KSPType ksptype;
      KSPGetType(_kspDistributed, &ksptype);
      PCType pctype;
      PCGetType(pc, &pctype);
      MatSolverType stype;
      PCFactorGetMatSolverType(pc, &stype);
      if(common::GmshFem::getMPIRank() == 0) {
        msg::info << "System size: " << m << " - "
                  << (ksptype ? ksptype : "none") << " "
                  << (pctype ? pctype : "none") << " "
                  << (stype ? stype : "none") << msg::endl;
      }

      KSPMatSolve(_kspDistributed, BlockRHS, BlockY);

      _loadPetscSolutionAllDistributed(BlockY, values, distributedContext);

      MatDestroy(&BlockRHS);
      MatDestroy(&BlockY);

      // Don't set fields to the solved system; loadSolution() should be used.
    }

    catch(const std::exception &exc) {
      values.clear();
      throw;
    }
#endif
  }

  template< class T_Scalar >
  void Solver< T_Scalar >::eigensolve(std::vector< scalar::ComplexPrecision< T_Scalar > > &eigenvalues, std::vector< std::vector< scalar::ComplexPrecision< T_Scalar > > > &eigenvectors, const bool computeEigenvectors, const unsigned int numberOfEigenvalues, const scalar::ComplexPrecision< T_Scalar > target)
  {
    //*******************
    // Solve :
    //    A x = \lambda x
    // or
    //    K x = \lambda M x
    // or
    //    K x = \lambda C x
    // or
    //    (\lambda^2 M + \lambda C + K) x = 0
    //*******************
    try {
#ifdef HAVE_SLEPC
      if(scalar::IsComplex< T_Scalar >::value && scalar::IsReal< PetscScalar >::value) {
        throw common::Exception("Eigensolve cannot be used on a complex formulation if PETSc if compiled in real arithmetic");
      }

      if(_A->getModule()->name() == "MCK") {
        msg::debug << "Using PEP solver" << msg::endl;
        PEP pep;
        PEPCreate(PETSC_COMM_SELF, &pep);

        Mat A[3];
        _A->getModule()->activate('K');
        A[0] = _A->getPetsc();
        _A->getModule()->activate('C');
        A[1] = _A->getPetsc();
        _A->getModule()->activate('M');
        A[2] = _A->getPetsc();
        PEPSetOperators(pep, 3, A);
        PEPSetProblemType(pep, (_A->getOptions().symmetric() && scalar::IsReal< T_Scalar >::value) || _A->getOptions().hermitian() ? PEP_HERMITIAN : PEP_GENERAL);
        PEPSetDimensions(pep, (numberOfEigenvalues == 0 ? _A->size() : numberOfEigenvalues), PETSC_DEFAULT, PETSC_DEFAULT);

        // Shift-and-invert spectral transformation toward target
        ST st;
        PEPGetST(pep, &st);
        STSetType(st, "sinvert");
        PEPSetTarget(pep, PetscScalar(target));
        PEPSetWhichEigenpairs(pep, PEP_TARGET_MAGNITUDE);

        // Use MUMPS
        KSP ksp;
        PC pc;
        STGetKSP(st, &ksp);
        KSPSetType(ksp, "preonly");
        KSPGetPC(ksp, &pc);
        PCSetType(pc, "lu");
        PCFactorSetMatSolverType(pc, "mumps");

        // Set from options
        PEPSetFromOptions(pep);

        PEPSolve(pep);
        PetscInt nconv = 0;
        PEPGetConverged(pep, &nconv);

        eigenvalues.resize(nconv);
        if(computeEigenvectors) {
          eigenvectors.resize(nconv);
        }

        // Extract eigenpairs
        Vec xr = nullptr, xi = nullptr;
        PetscScalar kr, ki;
        if(computeEigenvectors) {
          MatCreateVecs(_A->getPetsc(), &xr, &xi);
        }
        for(auto i = 0LL; i < nconv; ++i) {
          PEPGetEigenpair(pep, i, &kr, &ki, xr, xi);
#ifdef PETSC_USE_COMPLEX
          eigenvalues[i] = scalar::ComplexPrecision< T_Scalar >(PetscRealPart(kr), PetscImaginaryPart(kr));
          if(computeEigenvectors) {
            eigenvectors[i].resize(_A->size());

            PetscScalar *array;
            VecGetArray(xr, &array);
            for(auto j = 0ULL; j < _A->size(); ++j) {
              eigenvectors[i][j] = scalar::ComplexPrecision< T_Scalar >(PetscRealPart(array[j]), PetscImaginaryPart(array[j]));
            }
            VecRestoreArray(xr, &array);
          }
#else
          eigenvalues[i] = scalar::ComplexPrecision< T_Scalar >(kr, ki);
          if(computeEigenvectors) {
            eigenvectors[i].resize(_A->size());

            PetscScalar *arrayR, *arrayI;
            VecGetArray(xr, &arrayR);
            VecGetArray(xi, &arrayI);
            for(auto j = 0ULL; j < _A->size(); ++j) {
              eigenvectors[i][j] = scalar::ComplexPrecision< T_Scalar >(arrayR[j], arrayI[j]);
            }
            VecRestoreArray(xi, &arrayI);
            VecRestoreArray(xr, &arrayR);
          }
#endif // PETSC_USE_COMPLEX
        }

        VecDestroy(&xr);
        if(xi != nullptr) {
          VecDestroy(&xi);
        }
        MatDestroy(&A[0]);
        MatDestroy(&A[1]);
        MatDestroy(&A[2]);
        _A->removePetscData();

        PEPDestroy(&pep);
      }
      else {
        msg::debug << "Using EPS solver" << msg::endl;
        EPS eps;
        EPSCreate(PETSC_COMM_SELF, &eps);

        Mat A, B;
        EPSProblemType problemType = EPS_HEP;
        if(_A->getModule()->name() == "A") {
          A = _A->getPetsc();
          B = nullptr;
          problemType = ((_A->getOptions().symmetric() && scalar::IsReal< T_Scalar >::value) || _A->getOptions().hermitian() ? EPS_HEP : EPS_NHEP);
        }
        else if(_A->getModule()->name() == "MK" || _A->getModule()->name() == "CK") {
          _A->getModule()->activate(_A->getModule()->name()[1]);
          A = _A->getPetsc();
          _A->getModule()->activate(_A->getModule()->name()[0]);
          B = _A->getPetsc();
          problemType = ((_A->getOptions().symmetric() && scalar::IsReal< T_Scalar >::value) || _A->getOptions().hermitian() ? EPS_GHEP : EPS_GNHEP);
        }
        else {
          throw common::Exception("Unkown matrix module '" + _A->getModule()->name() + "'");
        }
        EPSSetOperators(eps, A, B);
        EPSSetProblemType(eps, problemType);
        EPSSetDimensions(eps, (numberOfEigenvalues == 0 ? _A->size() : numberOfEigenvalues), PETSC_DEFAULT, PETSC_DEFAULT);

        // Shift-and-invert spectral transformation toward target
        ST st;
        EPSGetST(eps, &st);
        STSetType(st, "sinvert");
        EPSSetTarget(eps, PetscScalar(target));
        EPSSetWhichEigenpairs(eps, EPS_TARGET_MAGNITUDE);

        // Use MUMPS
        KSP ksp;
        PC pc;
        STGetKSP(st, &ksp);
        KSPSetType(ksp, "preonly");
        KSPGetPC(ksp, &pc);
        PCSetType(pc, "lu");
        PCFactorSetMatSolverType(pc, "mumps");

        // Set from options
        EPSSetFromOptions(eps);

        // Solve
        EPSSolve(eps);
        PetscInt nconv = 0;
        EPSGetConverged(eps, &nconv);

        eigenvalues.resize(nconv);
        if(computeEigenvectors) {
          eigenvectors.resize(nconv);
        }

        // Extract eigenpairs
        Vec xr = nullptr, xi = nullptr;
        PetscScalar kr, ki;
        if(computeEigenvectors) {
          MatCreateVecs(_A->getPetsc(), &xr, &xi);
        }
        for(auto i = 0LL; i < nconv; ++i) {
          EPSGetEigenpair(eps, i, &kr, &ki, xr, xi);
#ifdef PETSC_USE_COMPLEX
          eigenvalues[i] = scalar::ComplexPrecision< T_Scalar >(PetscRealPart(kr), PetscImaginaryPart(kr));
          if(computeEigenvectors) {
            eigenvectors[i].resize(_A->size());

            PetscScalar *array;
            VecGetArray(xr, &array);
            for(auto j = 0ULL; j < _A->size(); ++j) {
              eigenvectors[i][j] = scalar::ComplexPrecision< T_Scalar >(PetscRealPart(array[j]), PetscImaginaryPart(array[j]));
            }
            VecRestoreArray(xr, &array);
          }
#else
          eigenvalues[i] = scalar::ComplexPrecision< T_Scalar >(kr, ki);
          if(computeEigenvectors) {
            eigenvectors[i].resize(_A->size());

            PetscScalar *arrayR, *arrayI;
            VecGetArray(xr, &arrayR);
            VecGetArray(xi, &arrayI);
            for(auto j = 0ULL; j < _A->size(); ++j) {
              eigenvectors[i][j] = scalar::ComplexPrecision< T_Scalar >(arrayR[j], arrayI[j]);
            }
            VecRestoreArray(xi, &arrayI);
            VecRestoreArray(xr, &arrayR);
          }
#endif // PETSC_USE_COMPLEX
        }

        VecDestroy(&xr);
        VecDestroy(&xi);
        if(_A->getModule()->name() == "A") {
          MatDestroy(&A);
        }
        else if(_A->getModule()->name() == "MK" || _A->getModule()->name() == "CK") {
          MatDestroy(&A);
          MatDestroy(&B);
        }
        _A->removePetscData();

        EPSDestroy(&eps);
      }
#else
      msg::error << "Unable to compute eigenvalues without SLEPc" << msg::endl;
#endif
    }
    catch(const std::exception &exc) {
      throw;
    }
  }

  template< class T_Scalar >
  PetscErrorCode Solver< T_Scalar >::getPreconditionerORAS(const DistributedContext< T_Scalar > &distributedContext, PC *out) const
  {
    #ifdef HAVE_PETSC
    PetscFunctionBeginUser;

    // Unpack distributed Context
    const std::vector< unsigned long long > &localIdOfOwned = distributedContext.localIDofOwned();
    const std::vector< unsigned long long > &localIdOfNonOwned = distributedContext.localIDofNonOwned();
    const std::vector< unsigned long long > &localToGlobalMap = distributedContext.localToGlobal();
    
    // Create PC and subdomains
    PC pc;
    PetscCall(PCCreate(MPI_COMM_WORLD, &pc));
    PetscCall(PCSetType(pc, PCASM));
    Mat A = _A->getPetscDistributed(distributedContext);
    PetscCall(PCSetOperators(pc, A, A));
    IS mySubdomain;
    distributedContext.getSubdomainsIS(&mySubdomain);
    PetscCall(PCASMSetLocalSubdomains(pc, 1, &mySubdomain, nullptr));

    // Setup optimized transmission
    ModifySubmatrixContext< T_Scalar > ctx;
    ctx.localIdOfNonOwned = &localIdOfNonOwned;
    ctx.localIdOfOwned = &localIdOfOwned;
    ctx.localToGlobalMap = &localToGlobalMap;
    ctx.A = _A;
    PetscCall(PCSetModifySubMatrices(pc, modifySubMatrix< T_Scalar >, &ctx));
    PetscCall(PCSetUp(pc));

    // Setup sub-solvers as MUMPS by default
    PetscInt nloc, firstloc;
    KSP *subksp;
    PetscCall(PCASMGetSubKSP(pc, &nloc, &firstloc, &subksp));
    PC subpc;
    PetscCall(KSPGetPC(subksp[0], &subpc));
    PetscCall(PCSetType(subpc, _forceCholesky ? PCCHOLESKY : PCLU));
    PetscCall(PCFactorSetMatSolverType(subpc, defaultSparseSolver()));

    *out = pc;
    PetscCall(PCSetUp(pc));

    PetscFunctionReturn(0);
    #else
    throw common::Exception("getPreconditionerORAS called but PETSc is not available");
    #endif
  }

  INSTANTIATE_CLASS(Solver, 4, TEMPLATE_ARGS(std::complex< double >, std::complex< float >, double, float))


} // namespace gmshfem::system
