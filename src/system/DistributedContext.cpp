// GmshFEM - Copyright (C) 2024, B. Martin, C. Geuzaine, Université de Liège
//
// See the LICENSE.txt file for license information. Please report all
// issues on https://gitlab.onelab.info/gmsh/fem/issues


#include "DistributedContext.h"
#include "PetscInterface.h"
#include "instantiate.h"

typedef struct _p_Mat *Mat;
typedef struct _p_IS *IS;
typedef struct _p_PetscSF *PetscSF;

namespace gmshfem::system
{

  template <class T_Scalar>
  class DistributedContextImpl
  {
   private:
#ifdef HAVE_PETSC
    IS _globalIds;
    Vec _distributedExtendedVector;
    Vec _distributedVector;
    VecScatter _scatter;
#endif
    DistributedContext< T_Scalar > &ctx;
   public:
    DistributedContextImpl(DistributedContext<T_Scalar>& ctx) : ctx(ctx) {
      #ifndef HAVE_PETSC
      throw gmshfem::common::Exception("PETSC is not available");
      #else

      unsigned nLoc = ctx.localIDofOwned().size();
      unsigned nOverlap = ctx.localIDofNonOwned().size();
      if (nLoc + nOverlap != ctx.localToGlobal().size())
        throw common::Exception("Wrong sizes");

      VecCreateMPI(PETSC_COMM_WORLD, nLoc + nOverlap, PETSC_DETERMINE, &_distributedExtendedVector);
      VecCreateMPI(PETSC_COMM_WORLD, nLoc, PETSC_DETERMINE, &_distributedVector);

      std::vector<PetscInt> mapping(nLoc+nOverlap);
      for (unsigned long long k = 0; k  < mapping.size(); ++k) {
        mapping[k] = ctx.localToGlobal()[k] - 1; // Global indices start to 1
      }

      ISCreateGeneral(PETSC_COMM_WORLD, nLoc+nOverlap, mapping.data(), PETSC_COPY_VALUES, &_globalIds);
      VecScatterCreate(_distributedVector, _globalIds, _distributedExtendedVector, nullptr, &_scatter);
      #endif
    }

    Vec doScatter(Vec distributedSol) {
      #ifndef HAVE_PETSC
      throw gmshfem::common::Exception("PETSC is not available");
      #else
      VecScatterBegin(_scatter, distributedSol, _distributedExtendedVector, INSERT_VALUES, SCATTER_FORWARD);
      VecScatterEnd(_scatter, distributedSol, _distributedExtendedVector, INSERT_VALUES, SCATTER_FORWARD);
      return _distributedExtendedVector;
      #endif
    }
    ~DistributedContextImpl() {
      #ifdef HAVE_PETSC
      VecDestroy(&_distributedExtendedVector);
      VecDestroy(&_distributedVector);
      VecScatterDestroy(&_scatter);
      ISDestroy(&_globalIds);
      #endif
    }
  };

  template <class T_Scalar>
  const typename DistributedContext<T_Scalar>::Indices &DistributedContext<T_Scalar>::localToGlobal() const
  {
    return _localToGlobal;
  }

  template <class T_Scalar>
  const typename DistributedContext<T_Scalar>::Indices &DistributedContext<T_Scalar>::localIDofOwned() const
  {
    return _localIDofOwned;
  }

  template <class T_Scalar>
  const typename DistributedContext<T_Scalar>::Indices &DistributedContext<T_Scalar>::localIDofNonOwned() const {
    return _localIDofNonOwned;
  }

  template< class T_Scalar >
  void DistributedContext< T_Scalar >::readScatteredData(std::vector<T_Scalar>& values, Vec sol) const
  {
    #ifdef HAVE_PETSC
    if (!_impl)
      throw common::Exception("Uninitialized DistributedContextImpl");
    Vec distSol = _impl->doScatter(sol);
    PetscInterface<T_Scalar, PetscScalar> interface;
    if (values.size() != _localToGlobal.size())
      throw common::Exception("Invalid values size " + std::to_string(values.size()) + " but expextected " + std::to_string(_localToGlobal.size()));
    PetscScalar* data;
    VecGetArray(distSol, &data);
    interface(values, data, static_cast<unsigned long long>(_localToGlobal.size()));
    VecRestoreArray(distSol, &data);
    #endif
  }

  #ifdef HAVE_PETSC
  template< class T_Scalar >
  PetscErrorCode DistributedContext< T_Scalar >::getSubdomainsIS(IS* is) const
  {
    PetscFunctionBeginUser;

    if (_subdomainsIS) {
      *is = _subdomainsIS;
      return PETSC_SUCCESS;
    }
    
    std::vector< PetscInt > isdata(localToGlobal().size());
    for(unsigned k = 0; k < localToGlobal().size(); ++k) {
      isdata[k] = localToGlobal().at(k) - 1;
    }
    PetscCall(ISCreateGeneral(PETSC_COMM_WORLD, isdata.size(), isdata.data(), PETSC_COPY_VALUES, &_subdomainsIS));
    *is = _subdomainsIS;

    PetscFunctionReturn(PETSC_SUCCESS);
  }
#endif

  template <class T_Scalar>
  DistributedContext<T_Scalar>::DistributedContext(Indices &&localToGlobal, Indices &&owned, Indices &&nonOwned) :
    _localToGlobal(std::move(localToGlobal)),
    _localIDofOwned(std::move(owned)), _localIDofNonOwned(std::move(nonOwned))
  {
    _impl = std::make_unique<DistributedContextImpl<T_Scalar>>(*this);
  }

  template <class T_Scalar>
  DistributedContext<T_Scalar>::~DistributedContext()
  {
    #ifdef HAVE_PETSC
    if (_subdomainsIS)
      ISDestroy(&_subdomainsIS);
    #endif
  }


  INSTANTIATE_CLASS(DistributedContext, 4, TEMPLATE_ARGS(std::complex< double >, std::complex< float >, double, float))

}; // namespace gmshfem::system
