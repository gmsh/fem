// GmshFEM - Copyright (C) 2024, B. Martin, C. Geuzaine, Université de Liège
//
// See the LICENSE.txt file for license information. Please report all
// issues on https://gitlab.onelab.info/gmsh/fem/issues

#ifndef H_GMSHFEM_MULTI_LEVEL_TOOLING
#define H_GMSHFEM_MULTI_LEVEL_TOOLING

#include "Memory.h"
#include "OmpInterface.h"
#include "Vector.h"
#include "gmshfemDefines.h"
#include "Formulation.h"

#include <string>
#include <vector>
#include <memory>

typedef struct _p_Vec *Vec;
typedef struct _p_Mat *Mat;

// TODO: ifdef everything with petsc

namespace gmshfem::system
{

template <class T_Scalar>
class DistributedContextImpl;

template <class T_Scalar>
class RestrictionLowOrder {
    using Indices = std::vector<unsigned long long>;

   private:
    Mat _globalRestriction = nullptr;
    Mat _localRestriction = nullptr;

   public:
    RestrictionLowOrder(problem::Formulation< T_Scalar > &formulation);
};

}


#endif