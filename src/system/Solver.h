// GmshFEM - Copyright (C) 2019-2022, A. Royer, E. Béchet, C. Geuzaine, Université de Liège
//
// See the LICENSE.txt file for license information. Please report all
// issues on https://gitlab.onelab.info/gmsh/fem/issues

#ifndef H_GMSHFEM_SOLVER
#define H_GMSHFEM_SOLVER

#include "Memory.h"
#include "gmshfemDefines.h"
#include "scalar.h"
#include "SolverMonitor.h"
#include "DistributedContext.h"

#include <vector>
#include <optional>
#include <memory>

typedef struct _p_KSP *KSP;
typedef struct _p_PC *PC;

namespace gmshfem::system
{
  template< class T_Scalar >
  class MatrixFactory;

  template< class T_Scalar >
  class VectorFactory;

  enum class DISTRIBUTED_SOLVE_TYPE;

  template< class T_Scalar >
  class RestrictionLowOrder;
} // namespace gmshfem::system

namespace gmshfem::system
{


  template< class T_Scalar >
  class Solver
  {
   private:
#ifdef HAVE_PETSC
    KSP _ksp, _kspDistributed;
    bool _kspDistributedInitialized = false;
    std::optional<double> _rtol;
#endif
    MatrixFactory< T_Scalar > *_A;
    std::vector< VectorFactory< T_Scalar > > &_b;
    std::unique_ptr<system::AbstractMonitor> _customMonitor;
    bool _mustUpdateOperator = true;
    bool _forceCholesky = false;

   protected:
#ifdef HAVE_PETSC
    void _loadPetscSolution(Vec x, std::vector< T_Scalar > &values);
    void _loadPetscSolutionDistributed(Vec x, std::vector< T_Scalar > &values, const DistributedContext< T_Scalar > &distributedContext);
    void _loadPetscSolutionAll(Mat x, std::vector< std::vector< T_Scalar > > &values);
    void _loadPetscSolutionAllDistributed(Mat x, std::vector< std::vector< T_Scalar > > &values, const DistributedContext< T_Scalar > &distributedContext);
    int _ensureCorrectOperator(Mat *A, Mat *B, bool reusePrec);
    int _ensureCorrectOperatorDistributed(Mat *A, Mat *B, bool reusePrec, const DistributedContext< T_Scalar > &distributedContext);
    static inline constexpr bool isComplexWithRealPetsc()
    {
      return scalar::IsComplex< T_Scalar >::value == true && scalar::IsComplex< PetscScalar >::value == false;
    }
        static const char *defaultSparseSolver();

public:
    void solveForPETSc(Vec x, Vec y, const bool reusePreconditioner = false);
    void solveForPETSc(Mat x, Mat y, const bool reusePreconditioner = false);
#endif


   public:
    void setMonitor(std::unique_ptr<system::AbstractMonitor>&& ptr);
    void mustUpdateOperator() { _mustUpdateOperator = true; }
    bool operatorMustBeUpdated() const { return _mustUpdateOperator; }
    void forceCholesky(bool chol) { _forceCholesky = chol; }
    void enforceRtol(std::optional<double> rtol);

    Solver(MatrixFactory< T_Scalar > *const A, std::vector< VectorFactory< T_Scalar > > &b);
    void initDistributedKSP();
    virtual ~Solver();

    common::Memory getEstimatedFactorizationMemoryUsage(bool chol = false) const;
    common::Memory getEstimatedFactorizationDistributedMemoryUsage(const DistributedContext<T_Scalar>& distributedContext, bool chol = false, bool sum = false) const;

    void solve(std::vector< T_Scalar > &values, const bool reusePreconditioner = false, unsigned rhsIdx = 0);
    void solveAll(std::vector< std::vector< T_Scalar > > &values, const bool reusePreconditioner = false);
    void solveDistributed(std::vector< T_Scalar > &values, DISTRIBUTED_SOLVE_TYPE, const DistributedContext< T_Scalar > &distributedContext, const bool reusePreconditioner);
    void solveAllDistributed(std::vector< std::vector< T_Scalar > > &values, DISTRIBUTED_SOLVE_TYPE, const DistributedContext<T_Scalar>& distributedContext, const bool reusePreconditioner = false);
    void eigensolve(std::vector< scalar::ComplexPrecision< T_Scalar > > &eigenvalues, std::vector< std::vector< scalar::ComplexPrecision< T_Scalar > > > &eigenvectors, const bool computeEigenvectors, const unsigned int numberOfEigenvalues, const scalar::ComplexPrecision< T_Scalar > target);
    PetscErrorCode getPreconditionerORAS(const DistributedContext< T_Scalar > &distributedContext, PC *out) const;
  };


} // namespace gmshfem::system

#endif // H_GMSHFEM_SOLVER
