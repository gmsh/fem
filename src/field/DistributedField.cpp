// GmshFEM - Copyright (C) 2023, B. Martin, C. Geuzaine, Université de Liège
//
// See the LICENSE.txt file for license information. Please report all
// issues on https://gitlab.onelab.info/gmsh/fem/issues

#include "DistributedField.h"

#include "Exception.h"
#include "GmshFem.h"
#include "Timer.h"
#include "instantiate.h"
#include "gmshfemDefines.h"

#include <CSVio.h>
#include <Options.h>
#include <gmsh.h>

#ifdef HAVE_MPI
#include <mpi.h>
#endif
#include <unordered_set>


namespace gmshfem::field
{

  using std::pair;
  using std::set;
  using std::unordered_set;
  using std::vector;

  template< class T_Scalar, field::Form T_Form >
  void DistributedField< T_Scalar, T_Form >::_computeDofsSubsets()
  {
    unsigned rank = gmshfem::common::GmshFem::getMPIRank();

    // Build set of inner and outer dofs, then iterative over values
    unordered_set< DofGlobalIndex, HashBySecond > inner, outer;

    msg::debug << '[' << rank << "] looks for gmsh dof indices for the inner part." << msg::endl;
    for(auto itEntity = _innerDomain.cbegin(); itEntity != _innerDomain.cend(); ++itEntity) {
      vector< int > elementTypes;
      gmsh::model::mesh::getElementTypes(elementTypes, itEntity->first, itEntity->second);

      for(auto elemType : elementTypes) {
        vector< int > typeKeys;
        vector< unsigned long long > entityKeys;
        vector< scalar::Precision< T_Scalar > > dummy;

        this->getFunctionSpace()->getKeys(false, typeKeys, entityKeys, dummy, elemType, *itEntity);

        for(unsigned i = 0; i < typeKeys.size(); ++i) {
          inner.insert({typeKeys[i], entityKeys[i]});
        }
      }
    }


    msg::debug << '[' << rank << "] looks for gmsh dof indices for the overlap part." << msg::endl;

    for(auto itEntity = _overlapDomain.cbegin(); itEntity != _overlapDomain.cend(); ++itEntity) {
      vector< int > elementTypes;
      gmsh::model::mesh::getElementTypes(elementTypes, itEntity->first, itEntity->second);

      for(auto elemType : elementTypes) {
        vector< int > typeKeys;
        vector< unsigned long long > entityKeys;
        vector< scalar::Precision< T_Scalar > > dummy;

        this->getFunctionSpace()->getKeys(false, typeKeys, entityKeys, dummy, elemType, *itEntity);

        for(unsigned i = 0; i < typeKeys.size(); ++i) {
          outer.insert({typeKeys[i], entityKeys[i]});
        }
      }
    }

    // For each dof in _values, add it to the corresponding section
    //for(auto hashmapIt = this->begin(); hashmapIt != this->end(); ++hashmapIt) {
    msg::debug << '[' << rank << "] iterates over actual dofs." << msg::endl;

    for(auto it = this->_pool.beginUnknownDofs(); it != this->_pool.endUnknownDofs(); ++it) {
      //dofs::Dof *dof = hashmapIt->first;
      dofs::Dof *dof = &(*it);

      DofGlobalIndex key{dof->numType() % GMSHFEM_DOF_FIELD_OFFSET, dof->entity()};
      DofIndex idx_tagged{dof->numType(), dof->entity()};
      bool isInner = inner.find(key) != inner.end();
      bool isOuter = outer.find(key) != outer.end();

      if(isInner) {
        _innerDofs.insert(dof);
        if(!isOuter)
          _ownedDofs.insert(dof);
        else
          _subdomainInterfaceDofs.insert(dof);
      }
      if(isOuter) {
        _sharedDofs.insert(dof);
      }
      if(isInner && isOuter) {
        _subdomainInterfaceDofs.insert(dof);
      }

      _indexToDof[idx_tagged] = dof;
    }

    // Overview
    msg::debug << '[' << rank << "] has " << _innerDofs.size() << " inner dofs, " << _sharedDofs.size() << " shared dofs and " << _subdomainInterfaceDofs.size() << " dofs on the interface." << msg::endl;
    if(gmshfem::common::Options::instance()->debug) {
      gmshfem::common::CSVio interfaceDofs("interface_" + std::to_string(rank));
      for(auto dof : _subdomainInterfaceDofs) {
        interfaceDofs << dof->numType() << dof->entity() << csv::endl;
      }
    }
  }


  template< class T_Scalar, field::Form T_Form >
  void DistributedField< T_Scalar, T_Form >::_synchronizeInterfaceOwnership()
  {
    /**
     * At the end of this:
     * - _ownedDofs contains all owned Dofs
     * - _toRead contains all the non owned Dofs
     * - _toSend is NOT filled
     * */
#ifdef HAVE_MPI

    struct ShareDofInfo {
      unsigned long long type;
      unsigned long long entity;
      unsigned this_rank;
    }; // One entry for each rank having this dof on the interface

    std::vector< ShareDofInfo > local, global;
    local.reserve(_subdomainInterfaceDofs.size());

    unsigned rank = gmshfem::common::GmshFem::getMPIRank();
    unsigned commSize = gmshfem::common::GmshFem::getMPISize();

    // Fill the local list
    for(dofs::Dof *dof : _subdomainInterfaceDofs) {
      local.push_back({dof->numType(), dof->entity(), rank});
    }

    // Get the sizes and offset of the global array
    int total_size = 0;
    std::vector< int > sizes(commSize), displs(commSize);
    int loc_size = local.size();
    MPI_Allgather(&loc_size, 1, MPI_INT, sizes.data(), 1, MPI_INT, MPI_COMM_WORLD);

    for(unsigned i = 0; i < commSize; ++i) {
      displs[i] = total_size;
      total_size += sizes[i];
    }

    // Create temporary MPI Type for the struct
    const int nitems = 3;
    int blocklengths[3] = {1, 1, 1};
    MPI_Datatype types[3] = {MPI_UNSIGNED_LONG, MPI_UNSIGNED_LONG, MPI_INT};
    MPI_Datatype mpi_struct_type;
    MPI_Aint offsets[3];
    offsets[0] = offsetof(ShareDofInfo, type);
    offsets[1] = offsetof(ShareDofInfo, entity);
    offsets[2] = offsetof(ShareDofInfo, this_rank);
    MPI_Type_create_struct(nitems, blocklengths, offsets, types, &mpi_struct_type);
    MPI_Type_commit(&mpi_struct_type);
    // Synchronize the full array
    global.resize(total_size);

    MPI_Allgatherv(local.data(), local.size(), mpi_struct_type,
                   global.data(), sizes.data(), displs.data(), mpi_struct_type,
                   MPI_COMM_WORLD);

    // Free the type
    MPI_Type_free(&mpi_struct_type);

    if (rank == 0)
      msg::info << "Synchronizing Interface Ownership : " << global.size() << " interface DOFs" << msg::endl;

    /**
     * global is now a list of pair (dof, rank). We can locally decide for each dof if we own it, using the min policy
    */

    // TODO: Key or Index ?
    std::unordered_map< DofGlobalIndex, decltype(rank), HashBySecond > ownershipData;
    for(const auto& entry: global) {
      DofGlobalIndex key{entry.type % GMSHFEM_DOF_FIELD_OFFSET, entry.entity};

      auto insertResult = ownershipData.insert({key, entry.this_rank});
      if(!insertResult.second) {
        // Already in, ensure it's the min
        auto it = insertResult.first;
        it->second = std::min(it->second, entry.this_rank);
      }
    }

    _toRead = this->_sharedDofs; // Copy the set (which incldues all the interface),
    // then REMOVE the owned part

    // Iterate again on interface dofs to add those that are owned
    for(dofs::Dof *dof : _subdomainInterfaceDofs) {
      auto match = ownershipData.find({dof->numType() % GMSHFEM_DOF_FIELD_OFFSET, dof->entity()});
      if(match == ownershipData.end())
        throw common::Exception("Missing interface ownership data!");

      if(match->second == rank) {
        _ownedDofs.insert(dof);
        _toRead.erase(dof);
      }
    }

#else
    throw common::Exception("MPI is not available");
#endif
  }

  template< class T_Scalar, field::Form T_Form >
  void DistributedField< T_Scalar, T_Form >::_computeToSend()
  {
#ifdef HAVE_MPI

    // Send all owned interfaces and inner dofs that are read by someone else
    std::vector< DofGlobalIndex > local, global;
    local.reserve(_sharedDofs.size());

    unsigned commSize = gmshfem::common::GmshFem::getMPISize();

    // Fill the local list
    for(dofs::Dof *dof : _toRead) {
      local.push_back({dof->numType(), dof->entity()});
    }

    // Get the sizes and offset of the global array
    int total_size = 0;
    std::vector< int > sizes(commSize), displs(commSize);
    int loc_size = local.size();
    MPI_Allgather(&loc_size, 1, MPI_INT, sizes.data(), 1, MPI_INT, MPI_COMM_WORLD);

    for(unsigned i = 0; i < commSize; ++i) {
      displs[i] = total_size;
      total_size += sizes[i];
    }

    // Create temporary MPI Type for the struct
    const int nitems = 2;
    int blocklengths[2] = {1, 1};
    MPI_Datatype types[2] = {MPI_INT, MPI_UNSIGNED_LONG};
    MPI_Datatype mpi_struct_type;
    MPI_Aint offsets[2];
    //offsets[0] = offsetof(DofGlobalIndex, type);
    //offsets[1] = offsetof(DofGlobalIndex, entity);
    offsets[0] = 0;
    offsets[1] = sizeof(int);

    MPI_Type_create_struct(nitems, blocklengths, offsets, types, &mpi_struct_type);
    MPI_Type_commit(&mpi_struct_type);
    // Synchronize the full array
    global.resize(total_size);

    MPI_Allgatherv(local.data(), local.size(), mpi_struct_type,
                   global.data(), sizes.data(), displs.data(), mpi_struct_type,
                   MPI_COMM_WORLD);

    // Free the type
    MPI_Type_free(&mpi_struct_type);

    _toSend.clear();
    // Global is the list of dof indices someone is reading
    for(DofGlobalIndex idx : global) {

      // For each read index, find my version of that dof. If it exists,
      // Check I own it
      auto myMatch = _indexToDof.find(idx);
      if(myMatch != _indexToDof.end()) {
        auto dof = myMatch->second;
        if(_ownedDofs.find(dof) != _ownedDofs.end())
          _toSend.insert(dof);
      }
    }
#else
    throw common::Exception("MPI is not available");
#endif
  }


  template< class T_Scalar, field::Form T_Form >
  void DistributedField< T_Scalar, T_Form >::preProMPI()
  {
    common::Timer timeSub;
    timeSub.tick();
    msg::debug << '[' << gmshfem::common::GmshFem::getMPIRank() << "] Computing Dofs subsets..." << msg::endl;
    _computeDofsSubsets();
    timeSub.tock();
    msg::info << "Dof subsets in " << timeSub << msg::endl;
    common::Timer timeOwnership;
    timeOwnership.tick();
    msg::debug << '[' << gmshfem::common::GmshFem::getMPIRank() << "] synchronizes ownership of shared Dofs..." << msg::endl;
    _synchronizeInterfaceOwnership();
    _computeToSend();
    timeOwnership.tock();
    msg::info << "Dof ownership in " << timeOwnership << msg::endl;
  }

  template< class T_Scalar, field::Form T_Form >
  std::unordered_set< dofs::Dof * > DistributedField< T_Scalar, T_Form >::getAllOwnedDofs()
  {
    return _ownedDofs;
  }

  template< class T_Scalar, field::Form T_Form >
  std::unordered_set< dofs::Dof * > DistributedField< T_Scalar, T_Form >::getNonOwnedDofs()
  {
    return _toRead;
  }

  template< class T_Scalar, field::Form T_Form >
  void DistributedField< T_Scalar, T_Form >::syncGlobalDofs(std::vector< unsigned long long > &localToGlobal, std::vector< unsigned long long > &readIDs)
  {
  #ifdef HAVE_MPI

    // To be called AFTER local dofs get numbered
    unsigned rank = gmshfem::common::GmshFem::getMPIRank();
    unsigned commSize = gmshfem::common::GmshFem::getMPISize();

    MPI_Barrier(MPI_COMM_WORLD);
    if(rank == 0)
      msg::debug << msg::endl
                 << "Entering syncGlobalDofs" << msg::endl;
    MPI_Barrier(MPI_COMM_WORLD);

    msg::debug << '[' << rank << "] is calling syncGlobalDofs. Will send " << _toSend.size() << msg::endl;

    struct LocalGlobalPair {
      unsigned long long type;
      unsigned long long entity;
      unsigned long long global;
    };

    std::vector< LocalGlobalPair > myOwnedDofs, allSharedDofsArray;

    for(auto dof : this->_toSend) {
      myOwnedDofs.push_back({dof->numType(), dof->entity(), dof->numGlobalDof()});
    }

    // Get the sizes and offset of the global array
    int total_size = 0;
    std::vector< int > sizes(commSize), displs(commSize);
    int loc_size = myOwnedDofs.size();
    MPI_Allgather(&loc_size, 1, MPI_INT, sizes.data(), 1, MPI_INT, MPI_COMM_WORLD);

    for(unsigned i = 0; i < commSize; ++i) {
      displs[i] = total_size;
      total_size += sizes[i];
    }


    MPI_Datatype types[3] = {MPI_UNSIGNED_LONG, MPI_UNSIGNED_LONG, MPI_UNSIGNED_LONG};
    MPI_Datatype mpi_struct_type;
    MPI_Aint offsets[3];
    int blocklengths[3] = {1, 1, 1};
    offsets[0] = offsetof(LocalGlobalPair, type);
    offsets[1] = offsetof(LocalGlobalPair, entity);
    offsets[2] = offsetof(LocalGlobalPair, global);
    MPI_Type_create_struct(3, blocklengths, offsets, types, &mpi_struct_type);
    MPI_Type_commit(&mpi_struct_type);
    // Synchronize the full array
    allSharedDofsArray.resize(total_size);

    MPI_Allgatherv(myOwnedDofs.data(), myOwnedDofs.size(), mpi_struct_type,
                   allSharedDofsArray.data(), sizes.data(), displs.data(), mpi_struct_type,
                   MPI_COMM_WORLD);

    if(rank == 0) {
      msg::info << "Gathered allSharedDofsArray. Total size is " << allSharedDofsArray.size() << msg::endl;
    }
    MPI_Barrier(MPI_COMM_WORLD);
    // Free the type
    MPI_Type_free(&mpi_struct_type);

    std::unordered_map< DofIndex, unsigned long long, HashBySecond, std::equal_to<DofIndex> > allSharedDofs;
    // Put in hashtable
    for (const auto &entry : allSharedDofsArray) {
      auto result = allSharedDofs.insert({{entry.type, entry.entity}, entry.global});
      if (result.second == false) {
        throw common::Exception("[" + std::to_string(rank) + "] Duplicate dof in allSharedDofs");
      }
    }


    for(dofs::Dof *readDof : _toRead) {
      DofIndex idx{readDof->numType(), readDof->entity()};
      auto firstOccurence = allSharedDofs.find(idx);
      if(firstOccurence == allSharedDofs.end())
        throw common::Exception("[" + std::to_string(rank) + "] Missing dof in allSharedDofs");

      dofs::Dof* dofObject = _indexToDof.at(idx);
      dofObject->numGlobalDof(firstOccurence->second);
      localToGlobal.at(dofObject->numDof() - 1) = dofObject->numGlobalDof();
      readIDs.push_back(dofObject->numDof());
    }

    MPI_Barrier(MPI_COMM_WORLD);
    if(rank == 0)
      msg::debug << msg::endl
                 << "Leaving syncGlobalDofs" << msg::endl;
    MPI_Barrier(MPI_COMM_WORLD);

#else
    throw common::Exception("MPI is not available");
#endif
  }

  template< class T_Scalar, field::Form T_Form >
  DistributedField< T_Scalar, T_Form >::DistributedField() :
    Field< T_Scalar, T_Form >()
  {
  }

  template< class T_Scalar, field::Form T_Form >
  DistributedField< T_Scalar, T_Form >::DistributedField(const std::string &name, const domain::Domain &innerdomain, const domain::Domain &outerdomain, const field::FunctionSpaceOfForm< T_Form > &type, const unsigned int order, const std::string &model) :
    Field< T_Scalar, T_Form >(name, innerdomain | outerdomain, type, order, model), _innerDomain(innerdomain), _overlapDomain(outerdomain)
  {
  }

  template< class T_Scalar, field::Form T_Form >
  DistributedField< T_Scalar, T_Form >::DistributedField(const std::string &name, const domain::Domain &innerdomain, const domain::Domain &outerdomain, const FunctionSpace< scalar::Precision< T_Scalar >, T_Form > &functionSpace, const std::string &model) :
    Field< T_Scalar, T_Form >(name, innerdomain | outerdomain, functionSpace, model), _innerDomain(innerdomain), _overlapDomain(outerdomain)
  {
  }

  template< class T_Scalar, field::Form T_Form >
  DistributedField< T_Scalar, T_Form >::DistributedField(const DistributedField< T_Scalar, T_Form > &other) :
    Field< T_Scalar, T_Form >(other), _innerDomain(other._innerDomain), _overlapDomain(other._overlapDomain)
  {
    this->_innerDofs = other._innerDofs;
    this->_sharedDofs = other._sharedDofs;
    this->_subdomainInterfaceDofs = other._subdomainInterfaceDofs;
    this->_ownedDofs = other._ownedDofs;
    this->_toRead =  other._toRead;
    this->_toSend = other._toSend;
    this->_localToGlobal = other._localToGlobal;
    this->_indexToDof = other._indexToDof;
  }

  template< class T_Scalar, field::Form T_Form >
  DistributedField< T_Scalar, T_Form >::~DistributedField()
  {
  }

  INSTANTIATE_CLASS_2(DistributedField, 4, 4, TEMPLATE_ARGS(std::complex< double >, std::complex< float >, double, float), TEMPLATE_ARGS(field::Form::Form0, field::Form::Form1, field::Form::Form2, field::Form::Form3))


} // namespace gmshfem::field
