// GmshFEM - Copyright (C) 2019-2022, A. Royer, E. Béchet, C. Geuzaine, Université de Liège
//
// See the LICENSE.txt file for license information. Please report all
// issues on https://gitlab.onelab.info/gmsh/fem/issues

#ifndef H_GMSHFEM_ANALYTICALOPERATIONS
#define H_GMSHFEM_ANALYTICALOPERATIONS

#include "AnalyticalNode.h"
#include "Helmholtz2D.h"
#include "Helmholtz3D.h"
#include "Maxwell2D.h"
#include "Maxwell3D.h"
#include "Navier2D.h"


#endif // H_GMSHFEM_ANALYTICALOPERATIONS
