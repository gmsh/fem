// GmshFEM - Copyright (C) 2019-2022, A. Royer, E. Béchet, C. Geuzaine, Université de Liège
//
// See the LICENSE.txt file for license information. Please report all
// issues on https://gitlab.onelab.info/gmsh/fem/issues

#ifndef H_GMSHFEM_FIELDNODEFUNCTIONS
#define H_GMSHFEM_FIELDNODEFUNCTIONS

#include "FieldNode.h"

namespace gmshfem::function
{


  void setPointEvaluation(ExecutionTreeIterator &it);


} // namespace gmshfem::function


#endif // H_GMSHFEM_FIELDNODE
